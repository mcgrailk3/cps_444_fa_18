set -o  trackall
#set -o vi
#set -o noclobber
cal
echo "\nHello Kevin, you look great today\n"
# -x is deprecated and has no effect
alias -x more="less"
unalias ls
alias -x ls="ls --color=auto -F"
alias -x l="ls -l"
alias -x ll="ls -l"
alias -x la="ls -a"
alias -x gr="go run"
alias -x gb="go build"
alias -x ch="cd homeworks"
alias -x rm="rm -i"
alias -x vi="vim"
unalias cd
alias -x cd='_(){ cd ${1} ; ls; }; _'

#alias -x print1="enscript -d LaserJetP4015"

. $HOME/.profile
