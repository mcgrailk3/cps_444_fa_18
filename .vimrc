set ruler showmode showmatch ts=3 expandtab tabstop=3 bg=dark
syntax on
set mouse=a
set number
colorscheme desert
map  <CR>:!aspell --dont-backup check %<CR>:e! %<CR>
nmap <C-J> vipgq
filetype plugin indent on
