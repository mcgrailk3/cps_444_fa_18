#include <unistd.h>

int main (int argc, char** argv) {
   int n = atoi(argv[1]);

   int i = 0;

   for (i = 0; i < n; i++)
      if (fork() == 0)
         execlp ("./client", "client", "req", "seq", NULL);
}
