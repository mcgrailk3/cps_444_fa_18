2009-10 Department of Computer Science Colloquium Series

Presentation Title: Cyber Research and Graduate Fellowships at
the Air Force Institute of Technology

Speaker:

Rusty Baldwin, Ph.D., P.E., CISSP

Associate Research Director,
Center for Cyberspace Research
Air Force Institute of Technology
Wright-Patterson AFB, OH
E-mail: rusty.baldwin@afit.edu
Friday, January 15, 2010, 3:00pm--4:00pm
201 Miriam Hall

When: Friday, January 15, 2009, 3:00pm-4:00pm

Where: 21A Miriam Hall (with refreshments to follow in 139 Anderson Hall)

Sponsor: Department of Computer Science, University of Dayton

Abstract:

The security of cyberspace which includes the Internet, the cellular network,
critical civilian infrastructure and fixed and mobile computing devices is poor
at best.  Furthermore, cyberspace has become an accepted domain of warfare in
the modern age.  The Center for Cyberspace Research (CCR), a National Security
Agency and Department of Homeland Security recognized Center of Excellence in
Cyberspace education and research, is dedicated to improving the security of
cyberspace by conducting cutting edge research in cyber forensics, cyber attack
attribution, cell phone protection and exploitation, network attack, 
defense, and many other exciting areas.  

Our research partners include National Laboratories (Sandia, Idaho, Pacific
Northwest), the National Security Agency, the Air Force Research Lab, the
National Air and Space Intelligence Center, and 15 other organizations.
Current research at CCR in the above areas will be presented.  In addition,
opportunities for research collaboration will be discussed.

Graduate fellowships which include full tuition, books, and a $25,000/year
stipend with no RA or TA duties are available at CCR.  These fellowships
include post-fellowship civilian employment with National Labs, law enforcement
agencies, and the U.S. Government.  No military service is required.

About the Speaker:

Dr. Baldwin is a Professor of Computer Engineering and the Associate Director
of the Center for Cyberspace Research.  He retired from the US Air Force in
2004 with 23 years of service and has been a faculty member at the Air Force
Institute of Technology since 1999.  He has supervised more than 50 masters and
nine doctoral research efforts and has received more than 12M in research
grants including three grants from the National Science Foundation.  His
research interests include cell phone protection and exploitation, wireless
sensor network protocols, computer security, data analysis, and experimental
design.  Dr. Baldwin is a deacon for the Archdiocese of Cincinnati.

For more information, visit http://cpscolloquium.udayton.edu.
