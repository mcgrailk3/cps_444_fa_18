BEGIN {
   prevline = ""
}{
   if (NR == 1 || $0 != prevline){
      print $0
      prevline = $0
   }
}
