#!/usr/bin/env ksh

sed 's/^\([A-Z][a-z-]*\)[,]\([A-Z][a-z-]*\)$/\2 \1/
     s/^.* //' guestlist

exit 0
