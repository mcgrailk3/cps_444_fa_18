BEGIN {
   prevline = ""
} {

   if (NR == 1 || $0 != prevline) {
/*      print NR */
      print $0
      prevline = $0
   }
}
