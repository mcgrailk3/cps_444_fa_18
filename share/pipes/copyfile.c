/* copyfile.c: ref. [USP] Chapter 4, Program 4.6, p. 100 */

#include <unistd.h>
#include "restart.h"
#define BLKSIZE 1024

/*
int copyfile(int fromfd, int tofd) {
   char buf[BLKSIZE]; 
   int bytesread, byteswritten;
   int totalbytes = 0;

   for (  ;  ;  ) {
      if ((bytesread = r_read(fromfd, buf, BLKSIZE)) <= 0)
         break;     
      if ((byteswritten = r_write(tofd, buf, bytesread)) == -1)
         break;
      totalbytes += byteswritten;
   }
   return totalbytes;
}
*/

main(int argc, char** argv) {

   int fd1 = fopen (argv[1], "r");
   int fd2 = fopen (argv[2], "w");
   
   copyfile (fileno(fd1), fileno(fd2));
}
