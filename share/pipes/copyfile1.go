package main

import (
    "fmt"
    "io"
    "os"
)

/* copyfilecontents copies the contents of the file named src to
   the file named by dst. The file will be created if it does not already exist.
   If the destination file exists, all it's contents will be truncated and
   replaced by the contents of the source file. */
func Copyfile(src, dst string) (err error) {

    in, err := os.Open(src)
    if err != nil {
       return
    }
    defer in.Close()

    out, err := os.Create(dst)
    if err != nil {
       return
    }
    defer out.Close()
    
    /* func() {
       cerr := out.Close()
       if err == nil {
          err = cerr
       }
    }() */
 
    if _, err = io.Copy(out, in); err != nil {
       return
    }

    // like an fflush()
    err = out.Sync()
    return
}

func main() {

    fmt.Fprintf(os.Stderr, "Copying %s to %s\n", os.Args[1], os.Args[2])

    err := Copyfile(os.Args[1], os.Args[2])

    if err != nil {
       fmt.Fprintf(os.Stderr, "Copyfile failed %q\n", err)
    } else {
         fmt.Fprintf(os.Stderr, "Copyfile succeeded\n")
      }
}
