/* cat.l */

%{
#include<stdio.h>
%}


%%

.	/* match any character except newline */ printf ("%s", yytext);

\n	/* match newline */  printf ("\n"); 

%%

/* called by lex when EOF reached */
int yywrap (void) {
   /* convention is to return 1 */
   return 1;
}

int main (void) {
   /* main entry point for lex */
   yylex();
   return 0;
}
