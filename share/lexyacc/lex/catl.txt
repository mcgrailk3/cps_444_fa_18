/* cat.l */

%{
#include<stdio.h>
%}


%%

.	/* match any character except newline */ printf ("%s", yytext);

\n	/* match newline */  printf ("\n"); 

%%

/* called by lex when EOF reached */
int yywrap (void) {
   /* convention is to return 1 */
   return 1;
}

int main (void) {
   /* main entry point for lex */
   yylex();
   return 0;
}
/* cat2.l (version 2) */

%%

.  ECHO;

\n ECHO;

%%

int main (int argc, char** argv) {
      printf (":%s:\n", argv[1]);
   if ((yyin = fopen (argv[1], "r")) == NULL)
      printf ("broken\n");
   if (yyin == stdin)
      printf ("here\n");
   else
      printf ("there\n");

   yylex();
   fclose (yyin);
   return 0;
}
/* cat3.l (version 3) */

%{
int cc=0;
%}

%%

.		{ cc++; ECHO; }

\n		{ cc++; ECHO; }

%%

int main (int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   fclose (yyin);
   printf ("%d characters\n", cc);
   return 0;
}
/* cat4.l (version 4) */
/* cat -n */

%{
int cc = 0;
int lineno = 0;
%}

%%

^.*\n    { cc += strlen(yytext);
           printf ("%d %s", ++lineno, yytext); }
%%

int main(int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   printf ("%d characters.\n", cc);
   fclose(yyin);
   return 0;
}
/* cat5.l (version 5) */
/* cat -n */

%option yylineno

%{
int cc = 0;
%}

%%
^.*\n	{ cc += strlen(yytext);
        printf("%4d\t%s", yylineno-1, yytext); }

%%

int main (int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   printf ("%d characters.\n", cc);
   fclose (yyin);
   return 0;
}
