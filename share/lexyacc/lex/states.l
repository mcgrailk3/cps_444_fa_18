%{
%}

%x ONE
%x TWO


%%

a {BEGIN ONE; printf("start a\n"); }

b {BEGIN TWO; printf("start b\n"); }

<TWO>a { printf ("two a\n"); BEGIN 0; }
<TWO>b { printf ("two b\n"); BEGIN 0; }
<ONE>a { printf ("one a\n"); BEGIN TWO; }
<ONE>b { printf ("one b\n"); BEGIN TWO; }

%%

int yywrap() {
   return 1;
}

int main() {
   yylex();
   return 0;
}
