%{
#include<string.h>
#define SIZE 100
char* yylval;
char buf[SIZE];
char* s = NULL;
%}

%x STRING


%%

\"     { BEGIN STRING; s = buf; }

<STRING>\\\" { *s++ = '\"'; printf ("escaped quote\n"); }

<STRING>\\\n { printf ("escaped newline\n"); }

<STRING>\\n { *s++ = '\n'; printf ("newline\n"); }

<STRING>\\t { *s++ = '\t'; printf ("tab\n"); }

<STRING>\"     { *s = '\0';
                 BEGIN 0;
                 printf (":%s:\n", buf); }

<STRING>\n { }

<STRING>. { *s++ = *yytext; }


. {}

\n {} 

%%

int warning(char* s) {
   fprintf (stderr, "%s", s);
}

int yywrap() {
   return 1;
}

int main(int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   int yy_flex_debug = 1;
   yylex();
   fclose(yyin);
   return 0;
}
