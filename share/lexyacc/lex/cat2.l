/* cat2.l (version 2) */

%%

.  ECHO;

\n ECHO;

%%

int yywrap (void) {
   return 1;
}

int main (int argc, char** argv) {
      printf (":%s:\n", argv[1]);
   if ((yyin = fopen (argv[1], "r")) == NULL)
      printf ("broken\n");
   if (yyin == stdin)
      printf ("here\n");
   else
      printf ("there\n");

   yylex();
   fclose (yyin);
   return 0;
}
