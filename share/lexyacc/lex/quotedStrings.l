%{
#include<string.h>
extern int yy_flex_debug;
char* yylval;
%}

%%

["][^"\n]*["]     { printf (":%s:\n", yytext);
                    yylval = strdup(yytext+1);
                    /* yylval[strlen(yylval)-1] = '\0'; */
                    yylval[yyleng-2] = '\0';
                    printf (":%s:\n", yylval); }

["][^"\n]*[\n]     { printf (":%s:\n", yytext);
                     warning("Invalid string:");
                     printf (":%s:\n", yytext+1); }

\n  { }
.   { }

%%

int yywrap() {
   return 1;
}

int warning (char* s) {
   fprintf (stderr, "%s\n", s);
   return 2;
}

int main(int argc, char** argv) {
   /* flex -d to enable debugging statements */
   yy_flex_debug = 1;
   yylex();
   return 0;
}
