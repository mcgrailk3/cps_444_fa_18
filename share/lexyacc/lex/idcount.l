alpha [_a-zA-Z]
alphanumeric [_a-zA-Z0-9]
digit [0-9]

%{
int idcount=0;
%}

%%

{alpha}({alpha}|{digit})* {idcount++; ECHO; printf("\n");}

. {}
\n {}

%%

int yywrap (void) {
   return 1;
}

int main (int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   fclose(yyin);
   printf ("This program contains %d identifiers.\n", idcount);
   return 0;
}
