%{
#include<string.h>
extern int yy_flex_debug;
char* yylval;
%}


%%

["][^"\n]*["\n]   { printf(":%s:\n", yytext);
                  yylval = strdup(yytext+1);
                  if (yylval[yyleng-2] == '"') {
                     yylval[yyleng-2] = '\0';
                  } else {
                     warning("invalid string\n");
                    }
                  printf(":%s:\n", yylval); }
 
. {}

\n {} 

%%

int warning(char* s) {
    fprintf (stderr, "%s", s);
}

int yywrap() {
   return 1;
}

int main(int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   int yy_flex_debug = 1;
   yylex();
   fclose(yyin);
   return 0;
}
