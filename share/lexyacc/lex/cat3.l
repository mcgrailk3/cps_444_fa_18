/* cat3.l (version 3) */

%{
int cc=0;
%}

%%

.		{ cc++; ECHO; }

\n		{ cc++; ECHO; }

%%

int yywrap (void) {
   return 1;
}

int main (int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   fclose (yyin);
   printf ("%d characters\n", cc);
   return 0;
}
