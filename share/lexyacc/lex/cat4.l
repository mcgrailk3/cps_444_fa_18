/* cat4.l (version 4) */
/* cat -n */

%{
int cc = 0;
int lineno = 0;
%}

%%

^.*\n    { cc += strlen(yytext);
           printf ("%d %s", ++lineno, yytext); }
%%

int yywrap() {
   return 1;
}

int main(int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   printf ("%d characters.\n", cc);
   fclose(yyin);
   return 0;
}
