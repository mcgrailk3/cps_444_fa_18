%{
extern int yy_flex_debug;
char buf[100];
char* s = NULL;
%}

%x INQUOTE

%%

^[ \t]+\n      { printf("Found blank line.\n"); /* ignore blank lines */ }
^\n      { printf("Found blank line.\n"); /* ignore blank lines */ }
^[ \t]+  { /* ignore leading whitespace */ }

\"            { BEGIN INQUOTE; s = buf; *s++ = '"'; }

<INQUOTE>\\\\ { fprintf(stdout, "just found an escaped backslash.\n"); }
<INQUOTE>\\\"  { /* same pattern as <INQUOTE>\\["] */
                 *s++ = '\"'; fprintf(stdout, "found escaped quote\n"); }
<INQUOTE>\\\n  { fprintf(stdout, "found escaped newline\n"); }
<INQUOTE>\\n   { *s++ = '\n'; fprintf(stdout, "found newline\n"); }
<INQUOTE>\\t   { *s++ = '\t'; fprintf(stdout, "found tab\n"); }

<INQUOTE>[^"\n]       { *s++ = *yytext;  }
<INQUOTE>["]	  { BEGIN 0; *s++ = '"'; *s = '\0';
		          printf ("\nFound valid string :%s:\n", buf); s=buf; }

<INQUOTE>\n    { BEGIN 0; *s = '\0';
                 fprintf (stdout, "Invalid quoted string :%s:\n", buf); s=buf;
                 /* exit(1); */ }


[^"\n]+\n       { *((yytext+yyleng)-1) = '\0';
                fprintf (stdout, "Invalid no quoted string :%s:\n", yytext); s=buf; }

%%

int yywrap() {
   return 1;
}

int main() {
   yy_flex_debug = 0;
   yylex();
   return 0;
}
