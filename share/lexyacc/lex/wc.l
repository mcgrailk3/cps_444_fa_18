%{
int cc = 0;
int wc = 0;
int lc = 0;
%}


%%

\n      { lc++; cc++; }

[ \t]   { cc++; }

[^ \t\n]+ { wc++; cc += yyleng; /* count anything but whitespace */ }

%%

int yywrap() {
   return 1;
}

int main(int argc, char** argv) {
   yyin = fopen (argv[1], "r");
   yylex();
   printf ("%8d%8d%8d\n", lc, wc, cc);
   fclose(yyin);
   return 0;
}
