%{
#include "calc2.tab.h"
%}

%%

[a-z]   { /* the position of the character in the alphabet 0..25 */
     yylval = *yytext - 'a';
     return VARIABLE; }

0   { yylval = atoi (yytext);
          return INTEGER; }

[1-9][0-9]*   { yylval = atoi (yytext);
                  return INTEGER; }

[-+()=/*^;\n]   { /* operators */ return *yytext;}

print      { /* operator */ return PRINT;}

[ \t]   { /* skip whitespace */ }

.   { /* anything else is an error */ yyerror ("invalid character");}

%%

int yywrap (void) {
   return 1;
}
