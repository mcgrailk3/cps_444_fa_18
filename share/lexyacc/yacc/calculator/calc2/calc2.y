%token INTEGER VARIABLE PRINT
%right '='
%left '+' '-'
%left '*' '/'
%right '^'

%{
#include<stdio.h>
#include<math.h>
#define SIZE 26
#define YYDEBUG 0
int symtab[SIZE];
%}

%%

program: program statement ';' '\n'
   | statement ';' '\n'
   ;

statement:
   expr	
   | PRINT expr	{ printf ("%d\n", $2); }
   | VARIABLE '=' expr	{ symtab[$1] = $3; }
   ;

expr:
   INTEGER
   | VARIABLE		{ $$ = symtab[$1]; }
   | '-' expr %prec '^' { $$ = $2*-1; }
   | expr '*' expr	{ $$ = $1 * $3; }
   | expr '/' expr	{ $$ = $1 / $3; }
   | expr '+' expr	{ $$ = $1 + $3; }
   | expr '-' expr	{ $$ = $1 - $3; }
   | expr '^' expr	{ $$ = pow ($1, $3); }
   | '(' expr ')'	{ $$ = $2; }
   ;

%%

int yyerror (char* s) {
   fprintf (stderr, "%s\n", s);
   return 0;
}

int main (void) {
#if YYDEBUG
  yydebug = 1;
#endif
   int i;
   for (i=0; i < SIZE; i++)
      symtab[i] = 0;
   yyparse();
   return 0;
}
