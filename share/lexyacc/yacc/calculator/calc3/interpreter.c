#include<stdio.h>
#include<math.h>
#include "calc3.h"
#include "calc3.tab.h"

int dfs(PTnode* nodePtr) {
   
   if (!nodePtr) return 0;

   switch (nodePtr->flag) {
   
      case literalFlag:
         return nodePtr->literalOrVariable;

      case variableFlag:
         return environment[nodePtr->literalOrVariable];

      case operatorFlag:
 
          switch (nodePtr->operator1.operatorLiteral) {
   
             case WHILE:
                while (dfs (nodePtr->operator1.operands[0]))
                   dfs(nodePtr->operator1.operands[1]);
                return 0;

             case IF:
                if (dfs (nodePtr->operator1.operands[0]))
                   dfs(nodePtr->operator1.operands[1]);
                else if (nodePtr->operator1.numOfOperands > 2)
                   dfs(nodePtr->operator1.operands[2]);
                return 0;
  
             case PRINT:
                printf ("%d\n", dfs (nodePtr->operator1.operands[0]));
                return 0;

             case ';':
                dfs (nodePtr->operator1.operands[0]);
                return dfs (nodePtr->operator1.operands[1]);
  
             case '=':
                return environment[nodePtr->operator1.operands[0]->literalOrVariable] = dfs (nodePtr->operator1.operands[1]);

             case UMINUS:
                return - dfs(nodePtr->operator1.operands[0]);

             case '+':
                return dfs(nodePtr->operator1.operands[0]) + dfs(nodePtr->operator1.operands[1]);

             case '-':
                return dfs(nodePtr->operator1.operands[0]) - dfs(nodePtr->operator1.operands[1]);

             case '*':
                return dfs(nodePtr->operator1.operands[0]) * dfs(nodePtr->operator1.operands[1]);

             case '/':
                return dfs(nodePtr->operator1.operands[0]) / dfs(nodePtr->operator1.operands[1]);

             case '^':
                return (int) pow(dfs(nodePtr->operator1.operands[0]), dfs(nodePtr->operator1.operands[1]));

             case '<':
                return dfs(nodePtr->operator1.operands[0]) < dfs(nodePtr->operator1.operands[1]);

             case '>':
                return dfs(nodePtr->operator1.operands[0]) > dfs(nodePtr->operator1.operands[1]);

             case GE:
                return dfs(nodePtr->operator1.operands[0]) >= dfs(nodePtr->operator1.operands[1]);

             case LE:
                return dfs(nodePtr->operator1.operands[0]) <= dfs(nodePtr->operator1.operands[1]);

             case NE:
                return dfs(nodePtr->operator1.operands[0]) != dfs(nodePtr->operator1.operands[1]);

             case EQ:
                return dfs(nodePtr->operator1.operands[0]) == dfs(nodePtr->operator1.operands[1]);
      }
   }
   return 0;
}
