%{
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h> /* provides access to the variable argument macros */
#include "calc3.h"
#define SIZE 26

PTnode* newOperatorNode(int oper, int nops, ...);
PTnode* newLiteralOrVariableNode(int literalOrVariable, PTnodeFlag flag);
void freePTnode(PTnode* nodePtr);
int dfs(PTnode* nodePtr);

void yyerror(char* s);

int environment[SIZE];      /* environment */

extern int yylineno;

%}

/* value stack will be an array of these YYSTYPE's */ 
%union {
   int literal;       /* literal value */
   char environI;     /* environment index */
   PTnode* nodePtr;   /* node pointer */
};
/* generates the following:

   typedef union {
      int literal;      
      char environI;
      PTnode* nodePtr;
   } YYSTYPE;
   extern YYSTYPE yylval;
 */
/* in other words, constants, variables, and nodes can
   be represented by yylval in the parser's value stack */

/* binds INTEGER to iValue in the YYSTYPE union */
/* associates token names with correct component of the YYSTYPE union */
/* to generate following code */
/* yylval.nodePtr = newLiteralOrVariableNode(yyvsp[0].literal); */

%token <literal> INTEGER
%token <environI> VARIABLE
%token WHILE IF PRINT
/* binds expr to nodePtr in the YYSTYPE union */
%type <nodePtr> stmt expr stmtlist

%nonassoc IFX
%nonassoc ELSE 
%left GE LE EQ NE '>' '<'
%left '+' '-'
%left '*' '/'
%right '^'
%nonassoc UMINUS

%%

program: code                      { exit(0); }
         ;

code: code stmt                { dfs($2); freePTnode($2); }
          | /* NULL */

stmt: ';'            { $$ = newOperatorNode(';', 2, NULL, NULL); }
      | expr ';'                   { $$ = $1; }
      | PRINT expr ';'    { $$ = newOperatorNode(PRINT, 1, $2); }
      | VARIABLE '=' expr ';'    { $$ = newOperatorNode('=', 2,
                           newLiteralOrVariableNode($1,variableFlag), $3); }
      | WHILE '(' expr ')' stmt { $$ = newOperatorNode(WHILE, 2, $3, $5); }
      | IF '(' expr ')' stmt %prec IFX { $$ = newOperatorNode(IF, 2, $3, $5); }
      | IF '(' expr ')' stmt ELSE stmt { $$ = newOperatorNode(IF, 3, $3, $5, $7); }
      | '{' stmtlist '}'    { $$ = $2; }
      ;

stmtlist: stmt               { $$ = $1; }
           | stmtlist stmt { $$ = newOperatorNode(';', 2, $1, $2); }
           ;

expr: INTEGER      { $$ = newLiteralOrVariableNode($1, literalFlag); }
      | VARIABLE  { $$ = newLiteralOrVariableNode($1, variableFlag); }
      | '-' expr %prec UMINUS { $$ = newOperatorNode(UMINUS, 1, $2); }
      | expr '+' expr        { $$ = newOperatorNode('+', 2, $1, $3); }
      | expr '-' expr      { $$ = newOperatorNode('-', 2, $1, $3); }
      | expr '*' expr { $$ = newOperatorNode('*', 2, $1, $3); }
      | expr '/' expr { $$ = newOperatorNode('/', 2, $1, $3); }
      | expr '^' expr { $$ = newOperatorNode('^', 2, $1, $3); }
      | expr '<' expr { $$ = newOperatorNode('<', 2, $1, $3); }
      | expr '>' expr { $$ = newOperatorNode('>', 2, $1, $3); }
      | expr GE expr  { $$ = newOperatorNode(GE, 2, $1, $3); }
      | expr LE expr  { $$ = newOperatorNode(LE, 2, $1, $3); }
      | expr NE expr  { $$ = newOperatorNode (NE, 2, $1, $3); }
      | expr EQ expr  { $$ = newOperatorNode (EQ, 2, $1, $3); }
      | '(' expr ')'        { $$ = $2; }
      ;

%%

/* returns a pointer to node for a constant */
/* parse tree constructed bottom-up */
PTnode* newLiteralOrVariableNode(int literalOrVariable, PTnodeFlag flag) {

   PTnode* nodePtr;

   /* allocate node */
   if ((nodePtr = malloc (sizeof (*nodePtr))) == NULL)
      yyerror ("out of memory");

   /* copy information */
   nodePtr->flag = flag;
   nodePtr->literalOrVariable = literalOrVariable;

   return nodePtr;
}

/* returns a pointer to a node for an operator */
/* when an operator is encountered, a node is allocated
   and pointers to previously allocated nodes are entered as operands */
PTnode* newOperatorNode (int operatorLiteral, int numOfOperands, ...) {

   /* argument pointer */
   /* for variable number of operands */
   va_list ap; 

   PTnode* nodePtr;

   int i;

   /* allocate node */
   if ((nodePtr = malloc (sizeof(*nodePtr))) == NULL)
      yyerror ("out of memory");

   /* copy information */
   nodePtr->flag = operatorFlag;
   nodePtr->operator1.operatorLiteral = operatorLiteral;
   nodePtr->operator1.numOfOperands = numOfOperands;

   va_start(ap, numOfOperands);

   if (((nodePtr->operator1.operands =
         malloc (numOfOperands * sizeof (PTnode*)))) == NULL)
      yyerror ("out of memory");

   for (i = 0; i < numOfOperands; i++)
      nodePtr->operator1.operands[i] = va_arg (ap, PTnode*);
   va_end (ap);

   return nodePtr;
}

void freePTnode(PTnode* nodePtr) {

   int i;

   if (!nodePtr)
      return;

   if (nodePtr->flag == operatorFlag) {

      for (i = 0; i < nodePtr->operator1.numOfOperands; i++)
         freePTnode(nodePtr->operator1.operands[i]);
         //freePTNode(*((nodePtr->operator1.operands)+i));
      if (nodePtr->operator1.operands)
         free(nodePtr->operator1.operands);
   }

   free(nodePtr); 
}

void yyerror(char* s) {
   fprintf(stderr, "line %d: %s\n", yylineno, s);
}

int main(void) {
   yyparse();
   return 0;
}
