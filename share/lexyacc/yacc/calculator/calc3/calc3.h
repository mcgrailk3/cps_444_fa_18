typedef enum { literalFlag, variableFlag, operatorFlag } PTnodeFlag;

/* operator node (i.e., internal node) */
typedef struct {
   int operatorLiteral;        /* operator */
   int numOfOperands;          /* number of operands */
   struct PTnode1** operands;  /* a pointer to an array of pointers
                                  to operands */
} OperatorNode;

typedef struct PTnode1 {

   PTnodeFlag flag;            /* flag indicating instance of union */

   union {
      int literalOrVariable;   /* value of integer literal or
                                  index into environment array;
                                  (i.e., leaf node) */
      OperatorNode operator1;  /* operator node */
   };
} PTnode;

extern int environment[26];
