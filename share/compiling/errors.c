/* ref. [USP] Chapter 2, Example 2.7, p. 28 */

#include<stdio.h>
#include<string.h>
#include<errno.h>

main() {

   int errnobakup;

   int fildes;

   if (close(fildes) == -1) {
      errnobakup = errno; 
//      fprintf (stderr, "Failed to close file: %s\n", strerror(errno));
      perror ("Failed to close the file.");
      errno = errnobakup;

   }
}
