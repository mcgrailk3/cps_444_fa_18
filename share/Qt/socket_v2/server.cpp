#include <QtWidgets>
#include <QTcpSocket>
#include <QDebug>

#include "server.h"

ListenServer::
ListenServer(QObject* parent)
	: QTcpServer(parent),
	  portNumber(0)
{
	// Intentionally empty: nothing to do.
}

bool
ListenServer::
listen()
{
	// Pick a random port on localhost
	bool success = QTcpServer::listen(QHostAddress::LocalHost,portNumber);
	portNumber = serverPort(); // Save the port number for resuming
	return success;
}

void
ListenServer::
incomingConnection(qintptr descriptor)
/* A QTcpSocket object cannot be moved between threads so we can't use
 * nextPendingConnection().  Instead we modify the incomingConnection() method,
 * which is a virtual method and is invoked by the parent class whenever a new
 * connection arrives.  Our version will emit our subclass's version of the
 * newConnection() signal which passes the descriptor of the pending
 * connection's dedicated socket.  The ServerView object will receive the signal
 * and pass the descriptor to the worker thread (along with the count).
 * */
{
	emit newConnection(descriptor);
	return;
}

//------------------------------------------------------------------------------

RespondServer::
RespondServer(qint64 arg_appPID,QObject* parent)
	: QObject(parent),
	  appPID(arg_appPID)
{
	connect(this,SIGNAL(newConnection(qintptr,int)),
	        this,SLOT(respond(qintptr,int)));
}

void
RespondServer::
dispatchClient(qintptr descriptor,int count)
{
	qDebug() << QThread::currentThreadId()
	         << "dispatching connection to thread.";
	emit newConnection(descriptor,count);
	return;
}

void
RespondServer::
respond(qintptr descriptor,int count)
{
	QTcpSocket client;
	QString    message;

	if(client.setSocketDescriptor(descriptor)) {
		message = QString::number(appPID) + " count: " + QString::number(count);

		qDebug() << QThread::currentThreadId() << "responding."
		         << "Wrote" << client.write(message.toLocal8Bit())
		         << "bytes to socket.";
		client.waitForBytesWritten();
		client.disconnectFromHost();
	}

	return;
}

//------------------------------------------------------------------------------

ServerView::
ServerView(QWidget* parent)
	: QMainWindow(parent),
	  instance(this),
	  worker(this),
	  responder(new RespondServer(QCoreApplication::applicationPid())),
	  count(1),
	  pauseButton(new QPushButton("&Pause"))
{
	// Widget objects
	QLabel*      message = new QLabel;
	QPushButton* quitButton = new QPushButton("&Quit");
	QHBoxLayout* buttonLayout = new QHBoxLayout;
	QVBoxLayout* centralLayout = new QVBoxLayout;
	QWidget*     centralWidget = new QWidget;

	/* Not all of Qt's types can be passed via signals by default.  In order to
	 * pass qintptr variables via the newConnection() signal, we must register
	 * it.
	 * */
	qRegisterMetaType<qintptr>("qintptr");

	/* All objects which descend from QObject implement an event loop which
	 * enables them to respond to signals.  responder is an object of the class
	 * RespondServer which inherits from QObject and thus has an event loop.
	 * When we call an object's moveToThread() method, Qt sets that object's
	 * thread affinity and thus its event loop is executed by the thread who's
	 * address is passed to moveToThread().  Be careful when using
	 * moveToThread(); the object you call it on must not have a parent object.
	 * RespondServer's constructor sets parent to NULL if no parent address is
	 * provided.  moveToThread() must also be called from the thread which is
	 * currently executing the object's event loop.  For more information, see:
	 *     http://qt-project.org/doc/qt-5/qobject.html#thread-affinity
	 *     http://qt-project.org/doc/qt-5/qobject.html#moveToThread
	 *
	 * Qt's documentation also has a good introduction to working with threads
	 * in Qt at:
	 *     http://qt-project.org/doc/qt-5/thread-basics.html
	 * */
	responder->moveToThread(&worker);
	worker.start();

	connect(&instance,SIGNAL(newConnection(qintptr)),
	        this,SLOT(respond(qintptr)));
	instance.listen();

	// Setup the main window
	message->setText("Listenting on 'localhost:" +
	                 QString::number(instance.serverPort()) + "'");
	connect(pauseButton,SIGNAL(clicked()),this,SLOT(pause()));
	connect(quitButton,SIGNAL(clicked()),this,SLOT(close()));

	buttonLayout->addWidget(pauseButton);
	buttonLayout->addWidget(quitButton);

	centralLayout->addWidget(message);
	centralLayout->addLayout(buttonLayout);
	centralWidget->setLayout(centralLayout);
	setCentralWidget(centralWidget);
	setWindowTitle("Socket Server");
}

ServerView::
~ServerView()
{
	instance.close();
	responder->deleteLater();
	worker.quit();
	worker.wait(); // Wait for the worker thread to finish cleaning up
}

void
ServerView::
respond(qintptr descriptor)
/* It is tempting to simply call into a RespondServer object to have our worker
 * thread respond to a client via a dedicated socket since we moved that object
 * to the worker thread in the constructor.  However, the function invocation
 * mechanism has no facility for invoking across thread boundries which would
 * result in the main thread executing the respond() method.  Instead of calling
 * RespondServer's respond() method directly, we call RespondServer's
 * dispatchClient() method which emits a signal that will be received by the
 * RespondServer object's event loop which is running on the worker thread.
 * */
{
	responder->dispatchClient(descriptor,count++);
}

void
ServerView::
pause()
{
	if(instance.isListening()) {
		instance.close();
		pauseButton->setText("&Restart");
	}
	else {
		instance.listen();
		pauseButton->setText("&Pause");
	}

	return;
}
