#ifndef SERVER_H
#define SERVER_H

#include <QMainWindow>
#include <QTcpServer>
#include <QThread>

// Qt forward declarations
class QPushButton;

class ListenServer : public QTcpServer
{
	Q_OBJECT

public:
	ListenServer(QObject* parent = NULL);

	bool listen();

signals:
	/* Override QTcpServer's newConnection() signal to pass the socket
	 * descriptor
	 * */
	void newConnection(qintptr descriptor);

protected:
	// Emits the newConnection() signal
	void incomingConnection(qintptr descriptor);

private:
	quint16 portNumber;
};

//------------------------------------------------------------------------------

class RespondServer : public QObject
{
	Q_OBJECT

public:
	RespondServer(qint64 arg_appPID,QObject* parent = NULL);

	void dispatchClient(qintptr descriptor,int count);

signals:
	void newConnection(qintptr descriptor,int count);

private slots:
	void respond(qintptr descriptor,int count);

private:
	RespondServer();

	qint64 appPID;
};

//------------------------------------------------------------------------------

class ServerView : public QMainWindow
{
	Q_OBJECT

public:
	ServerView(QWidget* parent = NULL);
	~ServerView();

private slots:
	void respond(qintptr descriptor);
	void pause();

private:
	// Forbid the use of the copy constructor and assignment operator
	ServerView(const ServerView& src);
	ServerView& operator=(const ServerView& src);

	ListenServer   instance;
	QThread        worker;
	RespondServer* responder;
	int            count;

	QPushButton* pauseButton;
};

#endif
