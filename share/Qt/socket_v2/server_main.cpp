#include <QApplication>
#include "server.h"

int
main(int argc,char* argv[])
{
	QApplication app(argc,argv);
	ServerView   window;

	window.show();

	return app.exec();
}
