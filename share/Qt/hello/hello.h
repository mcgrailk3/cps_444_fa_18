#ifndef HELLO_H
#define HELLO_H

#include <QMainWindow>

class HelloWindow : public QMainWindow
{
	Q_OBJECT

public:
	HelloWindow(QWidget* parent = NULL);

private slots:
	void close();
};

#endif
