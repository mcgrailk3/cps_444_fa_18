#include <QLabel>
#include <QMessageBox>
#include <QPushButton>
#include <QVBoxLayout>
#include "hello.h"

HelloWindow::
HelloWindow(QWidget* parent)
	: QMainWindow(parent)
{
	QWidget*     centralWidget = new QWidget;
	QVBoxLayout* centralLayout = new QVBoxLayout;
	QLabel*      helloLabel = new QLabel("Hi, everybody!");
	QPushButton* quitButton = new QPushButton("&Quit");

	connect(quitButton,SIGNAL(clicked()),this,SLOT(close()));

	helloLabel->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	centralLayout->addWidget(helloLabel);
	centralLayout->addWidget(quitButton);
	centralWidget->setLayout(centralLayout);
	setCentralWidget(centralWidget);
	setWindowTitle("Hello");
}

void
HelloWindow::
close()
{
	QMessageBox::information(this,"Goodbye","Goodbye, everybody!");
	QMainWindow::close();
	return;
}
