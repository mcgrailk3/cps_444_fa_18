#ifndef SERVER_H
#define SERVER_H

#include <QMainWindow>
#include <QTcpServer>

// Qt forward declarations
class QPushButton;

class ServerView : public QMainWindow
{
	Q_OBJECT

public:
	ServerView(QWidget* parent = NULL);
	~ServerView();

private slots:
	void respond();
	void pause();

private:
	// Forbid the use of the copy constructor and assignment operator
	ServerView(const ServerView& src);
	ServerView& operator=(const ServerView& src);

	QTcpServer instance;
	quint16    portNumber;
	qint64     appPID;
	int        count;

	QPushButton* pauseButton;
};

#endif
