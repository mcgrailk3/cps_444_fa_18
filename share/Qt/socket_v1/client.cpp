#include <QtWidgets>
#include <QHostAddress>
#include <QDebug>

#include "client.h"

Client::
Client(QWidget* parent)
	: QMainWindow(parent),
	  server(NULL),
	  portInput(new QLineEdit)
{
	// Widget objects
	QWidget*     central = new QWidget;
	QVBoxLayout* layout = new QVBoxLayout;
	QFormLayout* inputLayout = new QFormLayout;
	QHBoxLayout* buttonLayout = new QHBoxLayout;
	QPushButton* connectButton = new QPushButton("&Connect");
	QPushButton* quitButton = new QPushButton("&Quit");

	inputLayout->addRow("Server Port:",portInput);

	buttonLayout->addWidget(connectButton);
	buttonLayout->addWidget(quitButton);

	layout->addLayout(inputLayout);
	layout->addLayout(buttonLayout);

	connect(connectButton,SIGNAL(clicked()),this,SLOT(openConnection()));
	connect(quitButton,SIGNAL(clicked()),this,SLOT(close()));

	central->setLayout(layout);
	setCentralWidget(central);
	setWindowTitle("Socket Client");
}

Client::
~Client()
{
	if(server) {
		delete server;
	}
	delete portInput;
}

void
Client::
openConnection()
{
	if(!portInput->text().isEmpty()) { // Make sure the user provided a port number
		if(server) { // Ensure the client is currently handling a connection
			server->abort();
			delete server;
		}
		server = new QTcpSocket(this);
		/* When new data is added to the socket's buffer, the QTcpSocket object
		 * will emit the readyRead() signal.  If an error occurs it will emit
		 * the error() signal which passes the error message.
		 * */
		connect(server,SIGNAL(readyRead()),this,SLOT(readMessage()));
		connect(server,SIGNAL(error(QAbstractSocket::SocketError)),
		        this,SLOT(error(QAbstractSocket::SocketError)));

		server->connectToHost(QHostAddress::LocalHost,
		                      portInput->text().toUInt());
	}

	return;
}

void
Client::
readMessage()
{
	QString message = server->readAll();

	server->disconnectFromHost();
	QMessageBox::information(this,"Socket Client",message);

	delete server;
	server = NULL;

	return;
}

void
Client::
error(QAbstractSocket::SocketError /*error*/)
{
	QMessageBox::warning(this,"Socket Client",
	                     "Socket error: " + server->errorString());
	return;
}
