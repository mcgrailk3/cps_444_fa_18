#include <QtWidgets>
#include <QTcpSocket>
#include <QDebug>

#include "server.h"

ServerView::
ServerView(QWidget* parent)
	: QMainWindow(parent),
	  instance(this),
	  portNumber(0),
	  appPID(QCoreApplication::applicationPid()),
	  count(1),
	  pauseButton(new QPushButton("&Pause"))
{
	// Widget objects
	QWidget*     centralWidget = new QWidget;
	QVBoxLayout* centralLayout = new QVBoxLayout;
	QHBoxLayout* buttonLayout = new QHBoxLayout;
	QLabel*      message = new QLabel;
	QPushButton* quitButton = new QPushButton("&Quit");

	/* When a QTcpServer object receives an incoming connection, it will emit
	 * the newConnection() signal.  We connect the newConnection() signal with
	 * this class's respond() slot.  For a complete intro to Qt's signals and
	 * slots mechanism, see: http://qt-project.org/doc/qt-5/signalsandslots.html
	 * */
	connect(&instance,SIGNAL(newConnection()),this,SLOT(respond()));
	instance.listen(); // Chooses a random port on localhost
	portNumber = instance.serverPort();

	// Setup the main window
	message->setText("Listenting on 'localhost:" +
	                 QString::number(portNumber) + "'");
	/* QPushButtons emit the clicked() signal when a user clicks on them.  We
	 * implement the pause() slot below while the close() slot is provided by the
	 * parent class, QMainWindow.
	 * */
	connect(pauseButton,SIGNAL(clicked()),this,SLOT(pause()));
	connect(quitButton,SIGNAL(clicked()),this,SLOT(close()));

	buttonLayout->addWidget(pauseButton);
	buttonLayout->addWidget(quitButton);

	centralLayout->addWidget(message);
	centralLayout->addLayout(buttonLayout);
	centralWidget->setLayout(centralLayout);
	setCentralWidget(centralWidget);
	setWindowTitle("Socket Server");
}

ServerView::
~ServerView()
{
	instance.close();
}

void
ServerView::
respond()
{
	QTcpSocket* client = instance.nextPendingConnection();
	QString     message = QString::number(appPID) + " count: " +
	                      QString::number(count++);

	qDebug() << "Responding.  Wrote" << client->write(message.toLocal8Bit())
	         << "bytes to socket.";
	client->waitForBytesWritten();
	client->disconnectFromHost();
	client->deleteLater();

	return;
}

void
ServerView::
pause()
{
	if(instance.isListening()) {
		instance.close();
		pauseButton->setText("&Restart");
	}
	else {
		instance.listen(QHostAddress::LocalHost,portNumber);
		pauseButton->setText("&Pause");
	}

	return;
}
