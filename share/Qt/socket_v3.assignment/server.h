#ifndef SERVER_H
#define SERVER_H

#include <QMainWindow>
#include <QMutex>
#include <QQueue>
#include <QTcpServer>
#include <QWaitCondition>


// Qt forward declarations
template <typename T> class QList;
class QPushButton;
class QThread;

class ListenServer : public QTcpServer
{
	Q_OBJECT

public:
	ListenServer(QObject* parent = NULL);

	bool listen();
	void close(bool serverExit);
	qintptr nextClient(int& arg_count);

protected:
	void incomingConnection(qintptr descriptor);

private:
	Q_DISABLE_COPY(ListenServer)

	QMutex          critical;
	QWaitCondition  incoming;
	QQueue<qintptr> waitingClients;
	quint16         portNumber;
	bool            exiting;
	int             count;
};

//------------------------------------------------------------------------------

class RespondServer : public QObject
{
	Q_OBJECT

public:
	RespondServer(ListenServer& arg_instance,qint64 arg_appPID,
	              QObject* parent = NULL);

public slots:
	void loop();

private:
	RespondServer(); // Forbid use of the default constructor
	Q_DISABLE_COPY(RespondServer)

	ListenServer* instance;
	qint64        appPID;
};

//------------------------------------------------------------------------------

class ServerView : public QMainWindow
{
	Q_OBJECT

public:
	ServerView(QWidget* parent = NULL);
	~ServerView();

private slots:
	void pause();

private:
	Q_DISABLE_COPY(ServerView)

	ListenServer          instance;
	QList<RespondServer*> responderList;
	QList<QThread*>       pool;
	qint64                appPID;

	QPushButton* pauseButton;
};

#endif
