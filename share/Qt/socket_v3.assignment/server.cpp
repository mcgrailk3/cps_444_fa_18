#include <QtWidgets>
#include <QTcpSocket>
#include <QDebug>

#include "server.h"

ListenServer::
ListenServer(QObject* parent)
	: QTcpServer(parent),
	  critical(),
	  incoming(),
	  waitingClients(),
	  portNumber(0),
	  exiting(false),
	  count(1)
{
	// Intentionally empty: nothing to do.
}

bool
ListenServer::
listen()
{
	bool success = QTcpServer::listen(QHostAddress::LocalHost,portNumber);
	if(portNumber == 0) {
		portNumber = serverPort();
	}
	return success;
}

void
ListenServer::
close(bool serverExit)
{
	if(serverExit) {
		exiting = true;
		incoming.wakeAll(); // Wake all the threads so they can quit()
	}
	QTcpServer::close();
	return;
}

qintptr
ListenServer::
nextClient(int& arg_count)
/* This is the function where the worker threads enter the monitor and take the
 * next pending client descriptor.  arg_count is an out parameter.
 * */
{
	qintptr next;

	/* TODO: count and waitingClients need to be protected by a monitor.  Once
	 *       in the monitor a worker thread needs to get the next value of count
	 *       as well as dequeue the the descriptor of the next pending client.
	 *       If there are currently no pending clients, the worker thread should
	 *       block (or wait) until a new client connects.
	 *
	 *       Note: that when the close() method above is invoked and the server
	 *       is exiting (not just pausing), them member variable exiting is set
	 *       to true and all worker threads will be signaled (the wakeAll() in
	 *       close()).  This allows the main thread to call quit() on each
	 *       worker thread so the server can exit cleanly.
	 *
	 *       See the QMutext documentation :
	 *           http://doc.qt.io/qt-5/qmutex.html
	 *       and the QWaitCondition documentation:
	 *           http://doc.qt.io/qt-5/qwaitcondition.html
	 * */

	return next;
}

void
ListenServer::
incomingConnection(qintptr descriptor)
/* This is the function where the main thread enters the monitor and queues up
 * the next incomming client connection.  In this particular toy server, it is
 * very unlikely that more than one client descriptor will ever be waiting in
 * the queue and the increment operation is so cheap.  But a more expensive or
 * blocking operation and a busy server could cause all threads to be busy when
 * a series of new client connections arrive.
 * */
{
	/* TODO: Once in the monitor, the main thread needs to enqueue the
	 *       descriptor of the next pending client.  Before leaving the montior,
	 *       the main thread should notify (or wake) a single worker thread.
	 * */

	return;
}

//------------------------------------------------------------------------------

RespondServer::
RespondServer(ListenServer& arg_instance,qint64 arg_appPID,QObject* parent)
	: QObject(parent),
	  instance(&arg_instance),
	  appPID(arg_appPID)
{
	// Intentionally empty: nothing to do.
}

void
RespondServer::
loop()
{
	QTcpSocket client;
	qintptr    descriptor;
	int        count;
	QString    message;

	while(/* TODO: Wait for the next client (enter the monitor) */) {
		if(client.setSocketDescriptor(descriptor)) {
			message = QString::number(appPID) + " count: " +
			          QString::number(count);

			qDebug() << QThread::currentThreadId() << "responding."
			         << "Wrote" << client.write(message.toLocal8Bit())
			         << "bytes to socket.";
			client.waitForBytesWritten();
			client.disconnectFromHost();
		}
	}

	return;
}

//------------------------------------------------------------------------------

ServerView::
ServerView(QWidget* parent)
	: QMainWindow(parent),
	  instance(this),
	  responderList(),
	  pool(),
	  appPID(QCoreApplication::applicationPid()),
	  pauseButton(new QPushButton("&Pause"))
{
	int            threadCount = QThread::idealThreadCount();
	RespondServer* responder = NULL;
	QThread*       thread = NULL;
	// Widget objects
	QWidget*     centralWidget = new QWidget;
	QVBoxLayout* centralLayout = new QVBoxLayout;
	QHBoxLayout* buttonLayout = new QHBoxLayout;
	QLabel*      message = new QLabel;
	QPushButton* quitButton = new QPushButton("&Quit");

	for(int i = 0; i < threadCount; i++) {
		/* TODO: Spawn threads and append them to the pool.  Also, allocate a
		 *       RespondServer object for each thread, append it to
		 *       responderList and move it to the nascent thread.  Once the
		 *       thread is started, its event loop should invoke
		 *       RespondServer::loop().  When a QThread is started, it will emit
		 *       the started() signal.
		 *
		 *       See the QThread documentation:
		 *           http://doc.qt.io/qt-5/qthread.html
		 * */
	}
	instance.listen();

	// Setup the main window
	message->setText("Listenting on 'localhost:" +
	                 QString::number(instance.serverPort()) + "'");
	connect(pauseButton,SIGNAL(clicked()),this,SLOT(pause()));
	connect(quitButton,SIGNAL(clicked()),this,SLOT(close()));

	buttonLayout->addWidget(pauseButton);
	buttonLayout->addWidget(quitButton);

	centralLayout->addWidget(message);
	centralLayout->addLayout(buttonLayout);
	centralWidget->setLayout(centralLayout);
	setCentralWidget(centralWidget);
	setWindowTitle("Socket Server");
}

ServerView::
~ServerView()
{
	QList<RespondServer*>::iterator i;
	QList<QThread*>::iterator       j;

	instance.close(true);
	// Iterate over the RespondServer objects and schedule them for deletetion.
	for(i = responderList.begin(); i != responderList.end(); i++) {
		(*i)->deleteLater();
	}
	// Iterate over the thread pool and quit each one.
	for(j = pool.begin(); j != pool.end(); j++) {
		(*j)->quit();
		(*j)->deleteLater();
	}
}

void
ServerView::
pause()
{
	if(instance.isListening()) {
		instance.close(false);
		pauseButton->setText("&Restart");
	}
	else {
		instance.listen();
		pauseButton->setText("&Pause");
	}

	return;
}
