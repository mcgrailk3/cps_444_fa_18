#ifndef CLIENT_H
#define CLIENT_H

#include <QMainWindow>
#include <QTcpSocket>

// Qt forward declarations
class QLineEdit;

class Client : public QMainWindow
{
	Q_OBJECT

public:
	Client(QWidget* parent = NULL);
	~Client();

private slots:
	void openConnection();
	void readMessage();
	void error(QAbstractSocket::SocketError error);

private:
	// Forbid the use of the copy constructor and assignment operator
   Q_DISABLE_COPY(Client)

	QTcpSocket* server;

	QLineEdit* portInput;
};

#endif
