/* author: Perugini */

#include<stdio.h>
#include<string.h>

//struct {
struct route {
   int routeID;
   char descrp[25];
   //char* descrp;
};

typedef struct route Route;

/*
typedef struct {
   int routeID;
   char descrp[25];
   //char* descrp;
} Route;
*/

main() {
   Route route1;
   Route longroutes[10];
   Route* routePtr = NULL;

   //route1.descrp = malloc(sizeof(*route1.descrp)*26);

   scanf("%d", &route1.routeID);
   /* printf("this decimal is: %d", route1.routeID); */
   /* printf("this decimal is: %3d", route1.routeID); */
   //scanf("%s", route1.descrp);
   scanf("%25s", route1.descrp);

   //scanf ("%d%25s", &route1.routeID, route1.descrp);

   printf("%d\n", route1.routeID); 
   printf("%s\n", route1.descrp); 
   printf(":%s:\n", route1.descrp); 

   longroutes[3] = route1;

/* same as
   longroutes[3].routeID = route1.routeID;
   strcpy(longroutes[3].routeID, route1.descrp);
   not longroutes[3].routeID = route1.descrp; which just copies pointer
 */

   //b[3].descrp = malloc (sizeof(*longroutes[3].descrp)*26);

   strcpy (route1.descrp, "new");

   //printf ("%d", longroutes[3].routeID);
   //printf ("%3d", longroutes[3].routeID);

   longroutes[3].routeID = 156;
   longroutes[3].descrp[0] = ';';

   //route1.descrp[0] = 'a';
   printf ("%s\n", longroutes[3].descrp);

   printf ("%d\n", route1.routeID);
   printf ("%s\n", route1.descrp);

   routePtr = longroutes;

   printf("%d\n", (routePtr+3)->routeID);
   printf("%s\n", (routePtr+3)->descrp);

   printf("size of Route %d\n", sizeof(Route));
   printf("size of Route* %d\n", sizeof(Route*));
   printf("%x\n", routePtr);

   /* here is what is happening:
      first we have to cast the routePtr to a long so we
      can add 3 * the size of Route
      without doing pointer arithmetic; if we don't
      cast the route ptr to a long the compiler thinks
      we are adding 3 * the size of Route to a pointer (address) and does the
      translation for us;
  
      so once we cast routePtr to a long, now we can add 3 * size of Route,
      but then inorder to use the -> operator to get the routeID out,
      we need to cast that computed long back to a pointer to a Route;
  
      remember the lhs of the -> must always contain a ptr, not a long */
   printf("%d\n", ((Route*) ((long) routePtr+(sizeof(Route)*3)))->routeID);
   printf("%s\n", ((Route*) ((long) routePtr+(sizeof(Route)*3)))->descrp);
}
