/* author: Perugini */

package main

import (
    "fmt"
    "unsafe"
//    "strings"
)

type Route struct {
   routeID int
   descrp string
   //descrp[25] string
}

func main() {

   /* go uses type inference */

   var route1 Route
   var longroutes [10]Route
   var routePtr *Route
   //var err error

   //route1.descrp = malloc(sizeof(*route1.descrp)*26)

   // newslines in format string of scanf (e.g., "%d\n") must
   // match newlines in the input
   fmt.Scanf("%d", &route1.routeID)
   //fmt.Scanf("%c")
   //fmt.Scanf("%s", &route1.descrp)
   fmt.Scanf("%25s", &route1.descrp)
   //fmt.Scanf ("%d%25s", &route1.routeID, &route1.descrp)


   //fmt.Printf("this decimal is: %d\n", route1.routeID)
   //fmt.Printf("this decimal is: %3d\n", route1.routeID)

   fmt.Printf("%d\n", route1.routeID)
   fmt.Printf("%s\n", route1.descrp) 
   fmt.Printf(":%s:\n", route1.descrp)

   longroutes[3] = route1

   /* In C, ptr->field = (*ptr).field
      In Go, just use ptr.field */

/* same as
   longroutes[3].routeID = route1.routeID
   strcpy(longroutes[3].routeID, route1.descrp)
 */

   /* arrays are of a fixed size 
      slices are of a variable size */
   //b[3].descrp = malloc (unsafe.Sizeof(*longroutes[3].descrp)*26)

   route1.descrp = "new"

   fmt.Printf ("%d\n", longroutes[3].routeID)
   fmt.Printf ("%3d\n", longroutes[3].routeID)

   longroutes[3].routeID = 156
   //longroutes[3].descrp[0] = ';'
   longroutes[3].descrp = replaceAtIndex(longroutes[3].descrp, ';', 0);
   fmt.Printf ("%s\n", longroutes[3].descrp)

   //route1.descrp[0] = 'a'
   route1.descrp = replaceAtIndex(route1.descrp, 'a', 0);

   fmt.Printf ("%d\n", route1.routeID)
   fmt.Printf ("%s\n", route1.descrp)

   routePtr = &longroutes[0]

   /* Unlike C, Go has no pointer arithmetic. 

   fmt.Printf("%d\n", (routePtr+3)->routeID)
   fmt.Printf("%s\n", (routePtr+3)->descrp) */

   fmt.Printf("size of Route %d\n", unsafe.Sizeof(route1))
   fmt.Printf("size of Route* %d\n", unsafe.Sizeof(routePtr))

   // %x and %X do not work in place of %p
   fmt.Printf("%p\n", routePtr)
   //fmt.Printf("%x\n", routePtr)
   //fmt.Printf("%X\n", routePtr)
}

func replaceAtIndex(in string, r rune, i int) string {
   /* a string is a read-only slice of bytes */
   /* rune is essentially same as int32 */
   out := []rune(in)
   out[i] = r
   return string(out)
}
