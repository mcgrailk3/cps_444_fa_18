/* author: Perugini */

#include<stdio.h>
#include<string.h>

main() {

   double taxrate = 7.3, discountrate;
   char buyer[16];
   char seller[16];
   //char* buyer = "tom"; // strings declared this way are read-only
   //char* seller = "cat";
   //char* buyer = strdup("tom");
   //char* seller = strdup("cat");

   double* tmpPtr = &taxrate;

   buyer[0] = 't';
   buyer[1] = 'o';
   buyer[2] = 'm';
   buyer[3] = '\0';
   seller[0] = 'c';
   seller[1] = 'a';
   seller[2] = 't';
   seller[3] = '\0';

   printf ("%f\n", *tmpPtr);
   printf ("%10.2f\n", *tmpPtr);

   discountrate = *tmpPtr;

   printf ("%3.2f\n", discountrate);
   printf ("%x\n", tmpPtr);
   printf ("%X\n", tmpPtr);
   printf ("%x\n", &taxrate);

   if (tmpPtr == &taxrate)
      printf("Yes, they are equal.\n");

   strcpy (seller,buyer);

   printf ("result: %d\n", strcmp(seller,buyer));
   strcat (buyer,seller);
   printf(":%s:\n", buyer);
   printf("%d\n", strlen(buyer));
}
