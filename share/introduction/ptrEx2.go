package main

import "fmt"

func main() {
	taxrate := 7.3
	var discountrate float64

	var buyer string = "Name"
	var seller string

	var tmpPtr *float64

	tmpPtr = &taxrate

	fmt.Printf("%f\n", *tmpPtr)

	discountrate = *tmpPtr

	fmt.Printf("%f\n", discountrate)

	fmt.Printf("%x\n", &taxrate)

	fmt.Printf("%x\n", tmpPtr)

	seller = buyer

	fmt.Printf("%t\n", seller == buyer)

	buyer += seller

	fmt.Printf("%s\n", buyer)

	fmt.Printf("%d\n", len(buyer))
}
