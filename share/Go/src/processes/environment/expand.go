package main

import (
   "os"
   "fmt"
)

func main() {
    s := os.ExpandEnv("$PWD=1 $HOME=2")
    fmt.Printf("%s\n", s)
}
