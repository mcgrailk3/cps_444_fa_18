package main

import (
   "os"
   "fmt"
)

func main() {
    fmt.Printf("A is %s\n", os.Getenv("A"))
    fmt.Printf("B is %s\n", os.Getenv("B"))
    fmt.Printf("PATH is %s\n", os.Getenv("PATH"))
}
