package main

import (
   "os"
   "strings"
   "fmt"
)

func main() {
    fmt.Fprintf(os.Stderr, "The environment list follows:\n")

    for _, e := range os.Environ() {
        //fmt.Println(e)
        pair := strings.Split(e, "=")
        fmt.Printf("%s=%s\n", pair[0], pair[1])
    }
}
