package main

import (
   "os"
   "fmt"
)

func main() {
    fmt.Printf("%s\n", os.Getenv("PWD"))
    fmt.Printf("%s\n", os.Getenv("CAT"))
    os.Setenv("PATH", "/home/bin")
    fmt.Printf("%s\n", os.Getenv("PATH"))
}
