package main

import (
   "os"
   "fmt"
)

func main() {
    fmt.Fprintf (os.Stdout, "I am process %d.\n", os.Getpid());
    fmt.Fprintf (os.Stdout, "My parent is %d.\n", os.Getppid());
}
