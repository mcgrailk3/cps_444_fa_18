package main

import (
   "fmt"
   "io/ioutil"
   "os"
   "os/exec"
)

// error checking ommited for brevity,
// but we can use the usual err != nil pattern

// moreover, we can collect the StderrPipe results the same way as StdoutPipe

func main() {
   grepCmd := exec.Command("grep", "hello")

   grepIn, _ := grepCmd.StdinPipe()
   grepOut, _ := grepCmd.StdoutPipe()

   grepCmd.Start()

   grepIn.Write([]byte("hello grep\ngoodbye grep\nwill not grep this"))

   grepIn.Close()

   grepBytes, _ := ioutil.ReadAll(grepOut)

   grepCmd.Wait()

   fmt.Fprintf(os.Stderr, "$ grep hello\n")
   fmt.Print(string(grepBytes))
}
