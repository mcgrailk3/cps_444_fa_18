package main

import (
   "syscall"
   "os"
   "os/exec"
   //"fmt"
)

func main() {
    argv := os.Args[1:]

    abs_path_to_cmd, _ := exec.LookPath(os.Args[1])

    /* Go's analog of execv */
    execErr := syscall.Exec(abs_path_to_cmd, argv, os.Environ())

    panic(execErr)
}
