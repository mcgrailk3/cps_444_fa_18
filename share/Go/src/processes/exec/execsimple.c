/* execls.c: ref. [USP] Chapter 3, Program 3.4, p. 79 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
   printf ("I am process %ld.\n", (long) getpid());
   execl("/bin/ls", "ls", "-l", NULL);
   perror("Parent failed to exec ls");
}
