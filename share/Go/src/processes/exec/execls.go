package main

import (
   "syscall"
   "os"
   "os/exec"
   "fmt"
)

func main() {
    // which ls
    abs_path_to_ls, lookErr := exec.LookPath("ls")

    if lookErr != nil {
       panic(lookErr)
    }

    fmt.Fprintf(os.Stderr, "%s\n", abs_path_to_ls)

    argv := []string{"ls", "-a", "-l"}

    /* Go's analog of execv */
    execErr := syscall.Exec(abs_path_to_ls, argv, os.Environ())
    panic(execErr)
}
