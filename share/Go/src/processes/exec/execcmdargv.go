package main

import (
   "fmt"
   "strings"
   "syscall"
   "os"
   "os/exec"
)

func main() {
    argv := strings.Fields (os.Args[1])

    abs_path_to_cmd, _ := exec.LookPath(argv[0])

    fmt.Printf ("%s\n", abs_path_to_cmd)

    /* Go's analog of execv */
    execErr := syscall.Exec(abs_path_to_cmd, argv, os.Environ())

    panic(execErr)
}
