package main

import "os"
import "io"

func main() {
   buf := make([]byte, 1)
   //buf := make([]byte, 1024)

   var n int
   var err error

   for err != io.EOF {
      n, err = os.Stdin.Read(buf)

      if n > 0 {
         os.Stdout.Write(buf[0:n])
      }
   }
}
