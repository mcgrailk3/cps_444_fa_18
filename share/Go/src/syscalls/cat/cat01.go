package main

import (
    "os"
    "io"
    "log"
)

func main() {
   if _, err := io.Copy(os.Stdout, os.Stdin); err != nil {
      log.Fatal(err)
   }
}
