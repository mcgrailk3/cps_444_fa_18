package main

import (
    "os"
    "io"
    "log"
//    "fmt"
)

func main() {
   var fp *os.File
   var err error

   argc := len(os.Args[0:])

   if argc == 1 {
      if _, err := io.Copy(os.Stdout, os.Stdin); err != nil {
         log.Fatal(err) // same as call to fmt.Print(err) & os.Exit(1)
      }
   } else {
        for _,filename := range os.Args[1:] {
           fp,err = os.Open(filename)
           if err != nil {
              //log.Fatal(err)
              panic(err) // (1) control suspended,
                         // (2) deferred functions run, and
                         // (3) return to caller
           } else if _, err := io.Copy(os.Stdout, fp); err != nil {
                panic(err)
             }
        }
     }
}
