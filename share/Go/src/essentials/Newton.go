package main

import (
   "fmt"
   "math"
)

func Sqrt(x float64) float64 {
   new_guess_z := 20.1
   old_guess_z := 20.5

   for math.Abs(new_guess_z - old_guess_z) > 0.0001 {
      old_guess_z = new_guess_z
      new_guess_z = new_guess_z - (new_guess_z*new_guess_z-x)/(2*new_guess_z)
   }
   return new_guess_z
}

func main() {
   fmt.Println(Sqrt(16))
   fmt.Println(math.Sqrt(16))

   fmt.Println(Sqrt(100))
   fmt.Println(math.Sqrt(100))
}
