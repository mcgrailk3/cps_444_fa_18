package main

import "golang.org/x/tour/pic"

func Pic(dx, dy int) [][]uint8 {

   // allocate memory for a two-dimensioanl array
   a := make([][]uint8, dy)

   for i := 0; i < dy; i++ {
      a[i] = make([]uint8, dx)
   }
   
    for i := 0; i < dy; i++ {
      for j := 0; j < dx; j++ {
         switch {
            case j % 17 == 0:
               a[i][j] = 50 
            case j % 2 == 0:
               a[i][j] = 240
            case j % 1 == 0:
               a[i][j] = 175
            default:
               a[i][j] = 85
         }
      }
   }   
   return a
}

func main() {
   pic.Show(Pic)
}
