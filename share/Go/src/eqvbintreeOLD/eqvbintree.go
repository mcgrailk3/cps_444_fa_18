package main

import (
   "fmt"
   "golang.org/x/tour/tree"
)

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
   if t != nil {
      Walk(t.Left, ch)
      ch <- t.Value
      Walk(t.Right, ch)
   }
   return
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
   var i, j int
   c1 := make(chan int)
   c2 := make(chan int)
   go func() {
      Walk(t1, c1)
      close(c1)
   }()
   go func() {
      Walk(t2, c2)
      close(c2)
   }()

   for i = range c1 {
      fmt.Println("here")
      j = <-c2
      fmt.Printf("%d %d\n", i, j)
      if i != j {
         return false
      }
   }
   return true
}

func main() {

   ch := make(chan int)
   go Walk(tree.New(1), ch)
   for i := 0; i < 10; i++ {
      fmt.Printf("%d ", <-ch)
   }
   fmt.Printf("\n")

   if Same(tree.New(1), tree.New(1)) {
      fmt.Println("same")
   } else {
      fmt.Println("different")
   }
   if Same(tree.New(1), tree.New(2)) {
      fmt.Println("same")
   } else {
      fmt.Println("different")
   }
}
