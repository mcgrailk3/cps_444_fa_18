package main

import (
    "fmt"
    "os"
    "flag"
)

func main() {
   fmt.Fprintf (os.Stderr, "argc is %d\n", len(os.Args))
   fmt.Fprintf (os.Stderr, "Using os.Args:\n")

   for i, arg := range os.Args {
      fmt.Printf("arg %d is %s\n", i, arg)
   }

   p_opt := flag.Bool("p", false, "the p option does ... um ... something")
   q_opt := flag.Bool("q", false, "the q option does ... um ... something")

   flag.Parse()

   if *p_opt {
      fmt.Fprintf (os.Stderr, "-p option is on.\n")
   }
   if *q_opt {
      fmt.Fprintf (os.Stderr, "-q option is on.\n")
   }

   fmt.Fprintf (os.Stderr, "\n\n")
   fmt.Fprintf (os.Stderr, "Using flag.Args():\n")

   for i, arg := range flag.Args() {
      fmt.Printf("arg %d is %s\n", i, arg)
   }
}
