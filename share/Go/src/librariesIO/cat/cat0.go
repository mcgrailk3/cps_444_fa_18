package main

import (
    "bufio"
    "fmt"
    "os"
    "io"
)

func main() {

    reader := bufio.NewReader(os.Stdin)
    input, err := reader.ReadString('\n')

    for err != io.EOF {
       fmt.Printf("%s", input)
//       fmt.Printf("Input character is: %v\n", string([]byte(input)[0]))
//       fmt.Printf("Input character is: %c\n", input[0])
       input, err = reader.ReadString('\n')
    }
}
