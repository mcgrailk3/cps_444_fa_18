package main

import (
    "os"
    "io"
    "log"
    "fmt"
)

func main() {
   var fp *os.File
   var err error

   argc := len(os.Args[0:])

   if argc == 1 {
      if _, err := io.Copy(os.Stdout, os.Stdin); err != nil {
         log.Fatal(err)
      }
   } else {
        for _, filename := range os.Args[1:] {
           fp, err = os.Open(filename)
           if err != nil {
              //log.Fatal(err)
              fmt.Fprintf(os.Stderr, "%s: %s: No such file or directory.\n",
                          os.Args[0], filename)
              panic(err)
              //os.Exit(2)
           } else if _, err := io.Copy(os.Stdout, fp); err != nil {
                //log.Fatal(err)
                panic(err)
             }
           defer fp.Close()
        }
     }

   os.Exit(0)
}
