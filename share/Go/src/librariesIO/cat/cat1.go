package main

import (
    "fmt"
    "os"
    "io"
    "bufio"
)

func main() {
   argc := len(os.Args)

   if argc == 1 {
      filecopy(os.Stdin, os.Stdout)
   } else {
         for _,arg := range os.Args[1:] {
            fp,err := os.Open(arg)
            if err != nil {
                fmt.Fprintf (os.Stderr, "cat: file not found!\n")
            } else {
                 filecopy(fp) 
                 fp.Close()
              }
         }
     }
}

func filecopy(in *os.File, out *os.File) {

    reader := bufio.NewReader(in)

    input,err := reader.ReadString('\n')

    for err != io.EOF {
       fmt.Fprintf(out, "%s", input)
       input,err = reader.ReadString('\n')
    }
}
