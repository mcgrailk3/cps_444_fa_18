/* ref. [CPL] Chapter 7, 7.6, p. 163 with minor modifications by Perugini */

#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>

/* cat: version 2 */
int main (int argc, char** argv) {

   void filecopy (FILE* ifs, FILE* ofs);

   int exit_status = 0;

   char* pgm = *argv;

   char* s = malloc (sizeof(*s)*16);

   FILE* fp = NULL;
                                 
   if (argc == 1)
      filecopy (stdin, stdout);
   else
      while (--argc > 0)
         if ((fp = fopen (*(++argv), "r")) == NULL) {
            //fprintf (stderr, "%s: %s: No such file or directory\n", pgm, *argv);

            //fprintf (stderr, "%s: %s: %s.\n", pgm, *argv, strerror(errno));

            //perror("preprended message");
            sprintf (s, "%s: %s", pgm, *argv);
            perror(s);
            //exit (1);
            /* or use following line to continue processing */
            exit_status = 1;
         } else {
              filecopy (fp, stdout);
              fclose (fp);
           }

   if (ferror (stdout)) {
      fprintf (stderr, "%s: error writing stdout\n", pgm);
      //perror("error writing stdout.");
      exit_status = 2;
   }

   exit (exit_status);
}

void filecopy (FILE* ifp, FILE* ofp) {
   int c;

   while ((c = getc (ifp)) != EOF)
      putc (c, ofp);
}
