package main

import (
    "bufio"
    "fmt"
    "os"
    "io"
)

func main() {

    fin,err := os.Open(os.Args[1])

    reader := bufio.NewReader(fin)
    input, err := reader.ReadString('\n')

    for err != io.EOF {
       fmt.Printf("%s", input)
         // %v is value in default format
//       fmt.Printf("Input character is: %v\n", string([]byte(input)[0]))
//       fmt.Printf("Input character is: %c\n", input[0])
       input, err = reader.ReadString('\n')
    }
}
