package main

import (
    "fmt"
    "os"
)

func main() {
   fmt.Fprintf (os.Stderr, "argc is %d\n", len(os.Args))
   for i, arg := range os.Args {
      fmt.Printf("arg %d is %s\n", i, arg)
   }

   for _, arg := range os.Args {
      fmt.Printf("arg is %s\n", arg)
   }

   for i := 0, i < len(os.Args); i++ {
      fmt.Printf("arg is %s\n", os.Args[i])
   }
}
