package listlib

import (
   "time" 
   "errors")

// helpful notes for C programmers learning Go:

// no "static" keyword in Go, use lower case var names for unexported, private stuff
// no forward declarations in Go
// no header files in Go
// no typedef in Go
// no -> operator on Go; that is, you can also use dots with struct pointers, and
// the pointers are automatically dereferenced
// NULL in C is nil in Go, but usually we need no initial to nil in Go
// because it is done by default

type Data_t struct {
   Logged_time time.Time
   Str string
}

type List_t struct {
   item Data_t
   next *List_t
}
 
// global, private variables
var endlist List_t
var headptr *List_t
var tailptr *List_t
var travptrs_array [1]*List_t
var travptrs []*List_t = travptrs_array[0:]
var travptrs_size = 0

/* returns a non-negative integer key for traversing the data list
   each key value produces an independent traversal starting
   from the beginning of the list; when the key is no longer
   needed, the caller must free the key resources by calling
   freekey() */

/* return a nonnegative key if successful */
func Accessdata() (int,error) {
   if headptr == nil {
      return -1,errors.New("Can't access an empty list.")
   }
   if travptrs_size == 0 {
      travptrs[0] = headptr
      travptrs_size++
      return 0,nil
   }
   travptrs = append(travptrs, headptr)
   travptrs_size++
   return travptrs_size-1,nil
}

/* allocates node for data and inserts a copy of the data
   item at the end of the list

   if successful, returns 0,nil
   if unsuccessful, returns -1,error */
func Adddata (data Data_t) (int,error) {
   newnode := new(List_t)
   newnode.item = data

   if headptr == nil {
      headptr = newnode
   } else {
        tailptr.next = newnode
     }
   tailptr = newnode
   return 0,nil
}

/* copies the next item in the traversal of the list into
   a user-supplied buffer of type Data_t and sets datap.Str 

   assigns an empty string "" for the string field of *datap
   to signify that there are no more entries to examine
   no need to call freekey() at that point 

   if successful, returns nil
   if unsuccessful, returns error */
func Getdata (key int, datap *Data_t) error {
   var t *List_t

   if (key < 0) || (key >= travptrs_size) || (travptrs[key] == nil) {
      return errors.New("Out of range.")
   }

   /* end of list, set datap.Str to "" */
   if travptrs[key] == &endlist {
      datap.Str = ""
      travptrs[key] = nil

      /* reaching end of list natural condition, not an error */           
      return nil; 
   }

   t = travptrs[key]
   datap.Str = t.item.Str

   datap.Logged_time = t.item.Logged_time

   if t.next == nil {
      travptrs[key] = &endlist
   } else {
        travptrs[key] = t.next
     }
   return nil
}

/* frees key resources associated with key key 

   do not call freekey once you have reached the end of the list

   if successful, returns nil
   if unsuccessful, returns errno */

/* free list entry corresponding to key */
func Freekey (key int) error {

   if  (key < 0) || (key >= travptrs_size)  {
      return errors.New ("Key out of range.")
   }
   travptrs[key] = nil
   return nil
}
