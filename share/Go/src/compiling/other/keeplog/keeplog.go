package main

import (
   "io"
   "os"
   "fmt"
   "bufio"
   "strings"
   "compiling/other/keeplog/keeplog_helperfuns")

func main() {
   history := true
   argc := len(os.Args)

   if argc == 1 {
      history = false
   } else if (argc > 2) || (os.Args[1] == "history") {
        fmt.Fprintf(os.Stderr, "Usage: %s [history]\n", os.Args[0])
        os.Exit(1)
     }

   stdin := bufio.NewReader(os.Stdin)

   cmd, err := stdin.ReadString('\n')

   for err != io.EOF {
      cmd = strings.TrimSuffix(cmd, "\n")
      if history && (cmd != "history") {
         keeplog_helperfuns.Showhistory (os.Stdout)
      } else {
           _,err = keeplog_helperfuns.Runproc (cmd)
           if err != nil {
              panic ("Failed to execute command")
           }
        }
      cmd, err = stdin.ReadString('\n')
   }
   fmt.Printf ("\n\n>>>>>> The list of commands executed is:\n")
   keeplog_helperfuns.Showhistory (os.Stdout)
   os.Exit(0)
}
