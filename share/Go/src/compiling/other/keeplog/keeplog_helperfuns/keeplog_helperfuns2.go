package keeplog_helperfuns

import (
   "os"
 //  "os/exec"
   "fmt"
   "time"
   "strings"
   "compiling/libraries/listlib"
)

/* output the history list of the file f */
func Showhistory(f *os.File) {
   var data listlib.Data_t

   key,err := listlib.Accessdata()
   if err != nil  {
      fmt.Fprintf(f, "No history\n")
      return
   }

   err = listlib.Getdata(key, &data)

   for (err == nil) && (data.Str != "") {
      fmt.Fprintf(f, "Command: %s\nTime: %s\n\n",
                  data.Str, formattime(data.Logged_time))
      err = listlib.Getdata(key, &data)
   }
}

/* formats paramter t as "MM/DD/YYYY[ ]HH:MM:SS" */
func formattime(t time.Time) string {

    var months map[string]string

    months = make(map[string]string)

    months["Jan"] = "01"; months["Jan"] = "02"; months["Mar"] = "03"
    months["Apr"] = "04"; months["May"] = "05"; months["Jun"] = "06"
    months["Jul"] = "07"; months["Aug"] = "08"; months["Sep"] = "09"
    months["Oct"] = "10"; months["Nov"] = "11"; months["Dec"] = "12"

    const layout = "Jan 2 2006 15:04:05"
    timeslice := strings.Split(t.Format(layout), " ")
    timestr := months[timeslice[0]] + "/"

    if len(timeslice[1]) == 1 {
       timestr += "0"
    }
    timestr += timeslice[1] + "/" + timeslice[2] + " " + timeslice[3]
    //fmt.Println (timestr)
    return timestr
}
