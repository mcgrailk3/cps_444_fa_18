package keeplog_helperfuns

import (
//   "os"
   "os/exec"
   "fmt"
   "time"
//   "strings"
   "compiling/libraries/listlib"
)

/* execute cmd; store cmd and time in history list */
func Runproc(cmd string) (int,error) {
   execute := new(listlib.Data_t)
   execute.Logged_time = time.Now().Local()
   execute.Str = cmd

   cmdp := exec.Command("ksh", "-c", cmd)
   cmdpOut, err := cmdp.Output()

   /* command could not be executed */
   if err != nil {
      panic(err)
   }
   fmt.Print(string(cmdpOut))

   return listlib.Adddata(*execute)
}
