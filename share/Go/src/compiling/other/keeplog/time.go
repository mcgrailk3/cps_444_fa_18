package main

import (
   "os"
   "fmt"
   "time"
   "strings"
)

func main() {
    // format: "MM/DD/YYYY[ ]HH:MM:SS"
    fmt.Fprintf(os.Stderr, "the time is: %s\n", time.Now().Local())

    var timestr string
    var months map[string]string

    months = make(map[string]string)

    months["Jan"] = "01"; months["Feb"] = "02"; months["Mar"] = "03"
    months["Apr"] = "04"; months["May"] = "05"; months["Jun"] = "06"
    months["Jul"] = "07"; months["Aug"] = "08"; months["Sep"] = "09"
    months["Oct"] = "10"; months["Nov"] = "11"; months["Dec"] = "12"

    current_time := time.Now().Local()
    const layout = "Jan 2 2006 15:04:05"
    timeslice := strings.Split(current_time.Format(layout), " ")
    timestr = months[timeslice[0]] + "/"

    if len(timeslice[1]) == 1 {
       timestr += "0"
    }
    timestr += timeslice[1] + "/" + timeslice[2] + " " + timeslice[3]
    fmt.Println (timestr)
}
