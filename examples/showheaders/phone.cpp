#include "phone.h"
#include "scrio.h"
#include "fileio.h"


void main() {
  booktype book;
  int c;
  int line=0;
  scrinit();
  open(book,defaultbook);
  pbrefresh(book,line);
  while ((c=getkey())!='q') {
    switch (c) {
     case 'a': { add(book); pbrefresh(book,line); break; }
     case 'd': { del(book,line); pbrefresh(book,line); break; }
     case scrupkey: { scrup(book,line); break; }
     case scrdnkey: { scrdn(book,line); break; }
     case 'r': { pbrefresh(book,line); break; }
    }
  }
  save(book,defaultbook);
  scrshut();
}
