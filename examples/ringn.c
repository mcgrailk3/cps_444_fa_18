#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv) {
  
   int i; 
   int fd[2];
   int temp;
   pid_t haschild;
   pipe(fd);
   dup2(fd[0], 0);
   dup2(fd[1], 1);
   
   close(fd[0]);
   close(fd[1]);

   pipe(fd);
   haschild = fork();
   
   if(haschild > 0) 
      dup2(fd[1], 1);
   else if(!haschild)
      dup2(fd[0], 0);

   close(fd[0]);
   close(fd[1]);

/*
   for(i=1; i<10; i++) {
      write(STDOUT_FILENO, &i, 4);
      read(STDIN_FILENO, &temp, 4);
      fprintf(stderr, "%d\n", temp);
   }
*/

   for(i=1; i<10; i++) {
      printf("%d\n", i);
      fflush(stdout);
      scanf("%d", &i);
      fprintf(stderr, "%d\n", i);
   }
}
