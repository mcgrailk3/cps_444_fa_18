#
# .profile - Bourne Shell startup script for login shells
#
# see also sh(1), environ(7).
#

# .profile to be used with the Korn shell in CPS 444/544

export PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
PATH=$PATH:/sbin:/usr/sbin:/usr/games
PATH=$PATH:/usr/local/go/bin



#export LIBRARY_PATH=/home/perugini_cps444/share/lib
#export C_INCLUDE_PATH=/home/perugini_cps444/share/include
export LIBRARY_PATH=/home/FA_18_CPS444_14/lib

export C_INCLUDE_PATH=/home/FA_18_CPS444_14/include
export HOST=$( echo $(hostname) | awk -F. '{print $1}' )
export HOST=$(hostname)

# for use with the Korn shell in CPS 444/544
# export PS1='[! $USER@$HOST:$PWD]
export PS1='[Kevin@CPSSuse:$PWD]
> '

export MANPATH=/usr/share/man:/usr/local/man

# Setting TERM is normally done through /etc/ttys.  Do only override
# if you're sure that you'll never log in via telnet or xterm or a
# serial line.
# Use cons25l1 for iso-* fonts
# TERM=cons25;  export TERM
#export TERM=vt100
export TERM=xterm-color

BLOCKSIZE=K;    export BLOCKSIZE
HISTORY=64;     export HISTORY
PAGER=less;     export PAGER
MANPAGER=less;  export MANPAGER
VISUAL=emacs;      export VISUAL
EDITOR=vi;      export EDITOR
FCEDIT=vi;      export FCEDIT
CVSEDITOR=vi; export CVSEDITOR
EXINIT="set ruler showmode showmatch"; export EXINIT
export LANG="en_US.iso88591"

# set ENV to a file invoked each time sh is started for interactive use.
#ENV=$HOME/.bashrc; export ENV
ENV=$HOME/.kshrc; export ENV

export GOPATH=/home/FA_18_CPS444_14/go
umask 0007
