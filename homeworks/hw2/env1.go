/**********************************************************
/
/      filename:   env1.go
/
/   description:   The env utility is implemented in go,
/                  with the flag -i available to use. When
/                  -i is used, env ignores the environment
/                  inherited from the shell.
/
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #2
/
/      assigned:   August 30, 2018
/           due:   September 6, 2018
/
/ **********************************************************/

package main

import (
   "flag"
   "fmt"
   "os"
   "os/exec"
   "strings"
   "syscall"
)

func main() {
   iPtr := flag.Bool("i", false, "the l option does ... um ... something")
   flag.Parse()

   for n := 0; n < len(flag.Args()); n++ {
      if strings.Contains(flag.Args()[n], "-") {
         if strings.Compare(flag.Args()[n], "-") == 0 {
            for i := 0; i < len(flag.Args()); i++ {
               if strings.Contains(flag.Args()[i], "=") {
                  fmt.Printf("%s\n", flag.Args()[i])
               }
            }
            os.Exit(0)
         }
      }
   }

   //binary, lookErr := exec.LookPath("ls")
   //args := []string{}
   //if lookErr != nil {
   //panic(lookErr)
   //}
   env := os.Environ()
   // flag i used
   //strcpy := flag.Args()
   /*
      for l := 0; l < len(strcpy); l++ {
         if strings.Contains(flag.Args()[l], "ls") {
            fmt.Print("here1\n")
            path, lookErr := exec.LookPath(strcpy[l])
            if lookErr != nil {
               panic(lookErr)
            } else {
               fmt.Print("here2\n")
               fmt.Printf("%s here5\n", strcpy[l])
               syscall.Exec(path, strcpy, env)
               fmt.Print("here3\n")
               os.Exit(10)
            }

         } else {
            fmt.Print("here4\n")
            fmt.Printf("%s here\n", strcpy[l])
            copy(strcpy[l:], strcpy[l+1:])
            strcpy[len(strcpy)-1] = ""
            strcpy = strcpy[:len(strcpy)-1]
            fmt.Printf("%s here2\n", strcpy[l])
         }
      }
   */
   if len(flag.Args()) > 1 && !*iPtr {
      for l := 0; l < len(flag.Args()); l++ {
         path, lookErr := exec.LookPath(flag.Args()[l])
         if lookErr != nil {
            if strings.Contains(flag.Args()[l], "=") {//for n := 0; n < len(flag.Args()); n++ {

               fmt.Printf("%s\n", flag.Args()[l])
            } else {
               panic(lookErr)
            }

         } else {
            syscall.Exec(path, flag.Args(), env)
         }

      }
   } else if *iPtr {
      for l := 0; l < len(flag.Args()); l++ {
         path, lookErr := exec.LookPath(flag.Args()[l])
         if lookErr != nil {
            fmt.Printf("%s\n", flag.Args()[l])
         } else {
            syscall.Exec(path, flag.Args(), env)
         }
      }

   } else {
      //syscall.Exec(command, flag.Args(), os.Environ())
      //just env
      if len(flag.Args()) < 1 {
         for n := 0; n < len(env); n++ {
            fmt.Printf("%s\n", env[n])
         }
      } else { //env with arguments after with =
         for l := 0; l < len(flag.Args()); l++ {
            if strings.Contains(flag.Args()[l], "=") {
               env = append(env, flag.Args()[l])
            } else {
               path, lookErr := exec.LookPath(flag.Args()[l])
               if lookErr != nil {
                  fmt.Fprintf(os.Stderr, "env: '%s': No such file or directory\n", flag.Args()[l])
                  os.Exit(127)
               } else {
                  syscall.Exec(path, flag.Args(), env)
               }
            }
         }
         for n := 0; n < len(env); n++ {
            fmt.Printf("%s\n", env[n])
         }
      }
   }

}
