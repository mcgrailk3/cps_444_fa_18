/**********************************************************
/
/      filename:   barrierclient.go
/
/   description:   Stock barrierclient given in class
/
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #6
/
/      assigned:   October 11, 2018
/           due:   October 18, 2018
/
/ **********************************************************/

package main

import (
	"hw6/barrierclient/waitatbarrier"
	"os"
	"strconv"
)

func main() {

	i, _ := strconv.Atoi(os.Args[1])

	/* first barrier */
	waitatbarrier.Waitatbarrier("barrier", i, 1)

	/* second barrier */
	waitatbarrier.Waitatbarrier("barrier", i, 2)

	os.Exit(0)
}
