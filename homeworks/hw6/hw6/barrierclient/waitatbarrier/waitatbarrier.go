/**********************************************************
/
/      filename:   waitatbarrier.go
/
/   description:   Defines the barrier and how the pipes
/                  interact with the programs.
/
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #6
/
/      assigned:   October 11, 2018
/           due:   October 18, 2018
/
/ **********************************************************/

package waitatbarrier

import (
	"fmt"
	"log"
	"os"
	"syscall"
)

func handle(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Wait handle\n")
		log.Fatal(err)
	}
}

func Waitatbarrier(name string, i int, bid int) error {

	src1 := "/home/FA_18_CPS444_14/go/src/hw6/"
	//fmt.Fprintf(os.Stderr, "Line 38 Wait at barrier \n")
	fmt.Fprintf(os.Stderr, "i: %d, pid %d, ppid: %d: just arrived at barrier %d\n", i, syscall.Getpid(), syscall.Getppid(), bid)
	request, err := syscall.Open(src1+name+".request", syscall.O_WRONLY, 0)
	handle(err)

	data := []byte{'g'}

	n, err := syscall.Write(request, data)
	if n < 1 {
		//fmt.Fprintf(os.Stderr, "Error 49 Wait\n")
		handle(err)
	}
	//fmt.Fprintf(os.Stderr, "Line 51 Wait at barrier \n")

	handle(err)
	fmt.Fprintf(os.Stderr, "client %d just wrote %s in 1 byte to request pipe\n", i, string(data[:]))
	err = syscall.Close(request)
	handle(err)

	release, err := syscall.Open(src1+name+".release", syscall.O_RDONLY, 0)
	handle(err)

	r, err := syscall.Read(release, data)
	handle(err)
	if r < 1 {
		//fmt.Fprintf(os.Stderr, "Error 64 Wait\n")
		handle(err)
	}
	//fmt.Fprintf(os.Stderr, "client %d just read %s in 1 byte from the release pipe\n", i, string(data[:]))
	fmt.Fprintf(os.Stderr, "i: %d, pid %d, ppid: %d: just read %s in 1 byte from the release pipe\n", i, syscall.Getpid(), syscall.Getppid(), string(data))
	fmt.Fprintf(os.Stderr, "i: %d, pid %d, ppid: %d: just read passed barrier %d\n", i, syscall.Getpid(), syscall.Getppid(), bid)

	err = syscall.Close(release)
	handle(err)
	return (err)
}
