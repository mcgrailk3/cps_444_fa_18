#ifndef SCRIO_H
#define SCRIO_H

// #include <ncurses.h>
#include <curses.h>
// #include <cursesn.h>

#define scrupkey KEY_UP
#define scrdnkey KEY_DOWN

void error(char *s,...);
void scrinit();
void scrshut();
void add(booktype &book);
void del(booktype &book,int &line);
void scrup(booktype &book,int &line);
void scrdn(booktype &book,int &line);
void pbrefresh(booktype &book,int line);
int getkey();

#endif //SCRIO_H
