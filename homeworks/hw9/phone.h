#ifndef PHONE_H
#define PHONE_H

#define firstmax  15
#define lastmax   15
#define areamax    3
#define numbermax  7
#define desmax    33
#define defaultbook "phbk.dat"

struct entrytype {// +1's are for the NULL character
  char first[firstmax+1];
  char last[lastmax+1];
  char area[areamax+1];
  char number[numbermax+1];
  char des[desmax+1];
};

struct booktype {
  entrytype *entries;
  int size;
  char *fname;
};

#endif //PHONE_H
