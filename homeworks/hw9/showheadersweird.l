/**********************************************************
/
/      filename:   showheaders.l
/
/   description:   Gets C and CPP include statements
/                  and prints them
/
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #9
/
/      assigned:   November 20, 2018
/           due:   November 26, 2018
/
/ **********************************************************/

%{
   int cflag = 0;
   int uflag = 0;
   char* comment[100];
   char* uncomment[100];
   int cindx = 0;
   int uindx = 0;
%}

%x INCL
%x CCOMMENT
%x CPPCOMMENT

%%

#include\ [<"] {BEGIN INCL; printf("Found #include \n"); }
\/\/ {BEGIN CCOMMENT; printf("Found // \n"); }
\/\* {BEGIN CPPCOMMENT; printf("Found /* \n"); }

<INCL>[>"] {BEGIN 0; uindx++;}
<INCL>. { uncomment[uindx] = strdup(yytext); }

<CCOMMENT>.* { }
<CCOMMENT>\n { comment[cindx] = strdup(yytext); cindx++; BEGIN 0; }

<CPPCOMMENT>.*\*\/\n { comment[cindx] = strdup(yytext); cindx++;  BEGIN 0; }

\n    { }
.     { }

%%

int yywrap (void) {
   return 1;
}

int main (int argc, char** argv) {
   int c;

   while ((c = getopt (argc, argv, "cu")) != -1){
      switch (c)
      {
      case 'c':
         cflag = 1;
         break;
      case 'u':
         uflag = 1;
         break;
      case '?':
         printf("./showheaders: Illegal option -%c\n", optopt);
         printf("Usage:  showheaders [-cu] [file(s)...]\n");
         exit(1);
      }
   }

   if ((cflag == 0) && (uflag == 0)) {
      cflag = 1; uflag = 1;
   }

   if (optind < argc) {
      while (optind < argc){
         yyin = fopen (argv[optind], "r");
         if (yyin == 0) {     
            printf("./showheaders: Invalid file %s\n", argv[optind]);
            printf("Usage:  showheaders [-cu] [file(s)...]\n");
            exit(1);
         } else {
            optind++;
         }
      }
   }

   yylex();
   fclose(yyin);
   int i=0;

   if (uflag = 1){
      printf("Uncommented header filename(s):\n");
      for(i=0; i < uindx; i++) {
         printf("%s\n", uncomment[i]);
      }
   }
   if (cflag = 1){
      printf("Commented header filename(s):\n");
      for(i=0; i < cindx; i++) {
         printf("%s\n", comment[i]);
      }
   }
   return 0;
}


