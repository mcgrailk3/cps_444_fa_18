#include "list.h"

#include <string.h>
#include <curses.h> //debug

void addentry(booktype &book,entrytype *entry) {
  entrytype *temp;
  int entnum=0;
  temp=new entrytype[book.size+1];
  while (entnum<book.size && strcmp(book.entries[entnum].last,entry->last)<=0) {
    strcpy(temp[entnum].last,book.entries[entnum].last);
    strcpy(temp[entnum].first,book.entries[entnum].first);
    strcpy(temp[entnum].area,book.entries[entnum].area);
    strcpy(temp[entnum].number,book.entries[entnum].number);
    strcpy(temp[entnum].des,book.entries[entnum].des);
    entnum++;
  }
  strcpy(temp[entnum].last,entry->last);
  strcpy(temp[entnum].first,entry->first);
  strcpy(temp[entnum].area,entry->area);
  strcpy(temp[entnum].number,entry->number);
  strcpy(temp[entnum].des,entry->des);
  entnum++;
  book.size++;
  for (;entnum<book.size;entnum++) {
    strcpy(temp[entnum].last,book.entries[entnum-1].last);
    strcpy(temp[entnum].first,book.entries[entnum-1].first);
    strcpy(temp[entnum].area,book.entries[entnum-1].area);
    strcpy(temp[entnum].number,book.entries[entnum-1].number);
    strcpy(temp[entnum].des,book.entries[entnum-1].des);
  }
  delete (book.entries);
  book.entries=temp;
}

void delentry(booktype &book,int &line) {
  int i;
  for (i=line;i<book.size-1;i++) {
    strcpy(book.entries[i].last,book.entries[i+1].last);
    strcpy(book.entries[i].first,book.entries[i+1].first);
    strcpy(book.entries[i].area,book.entries[i+1].area);
    strcpy(book.entries[i].number,book.entries[i+1].number);
    strcpy(book.entries[i].des,book.entries[i+1].des);
  }
  book.size--;
  if (line>=book.size) line=book.size-1;
}
