#include <stdio.h>
#include <string.h>
#include <malloc.h>

#include "phone.h"
#include "fileio.h"
#include "scrio.h"

void save(booktype &book,char *name=NULL) {
  FILE *f;
  int i;
  if (name==NULL) { name=book.fname; }
  else {
    delete book.fname;
    book.fname=new char[strlen(name)+1];
    strcpy(book.fname,name); 
  }
  if ((f=fopen(name,"w+"))==NULL) {
    error("filio.cpp: save: error opening %s",name);
    getkey();
    return;
  } else {
    fprintf(f,"%i\n",book.size);
    for (i=0;i<book.size;i++) {
      fprintf(f,"%s ",book.entries[i].first);
      fprintf(f,"%s ",book.entries[i].last);
      fprintf(f,"%s ",book.entries[i].area);
      fprintf(f,"%s ",book.entries[i].number);
      fprintf(f,"%s\n",book.entries[i].des);
    }
    fclose(f);
  }
}

void open(booktype &book,char *name=NULL) {
  FILE *f;
  int i;
  //  free(book.entries);
  if (name==NULL) { name=book.fname; }
  else { 
    book.fname=new char[strlen(name)+1];
    strcpy(book.fname,name); 
  }
  if ((f=fopen(name,"r"))==NULL) {
    error("filio.cpp: open: error opening %s\n",name);
    getkey();
    book.size=0;
    book.entries=new entrytype[0];
    return;
  } else {
    fscanf(f,"%i",&book.size);
    if ((book.entries=new entrytype[book.size])==NULL) {
      printf("fileio.cpp: open: error allocating memory (%i entries) for book\n",book.size);
      getchar();
      return;
    }
    for (i=0;i<book.size;i++) {
      fscanf(f,"%s",book.entries[i].first);
      fscanf(f,"%s",book.entries[i].last);
      fscanf(f,"%s",book.entries[i].area);
      fscanf(f,"%s",book.entries[i].number);
      strcpy(book.entries[i].des,"");
      fscanf(f,"%[^\n]s",book.entries[i].des);
    }
    fclose(f);
  }
}
#include "list.h"

#include <string.h>
#include <curses.h> //debug

void addentry(booktype &book,entrytype *entry) {
  entrytype *temp;
  int entnum=0;
  temp=new entrytype[book.size+1];
  while (entnum<book.size && strcmp(book.entries[entnum].last,entry->last)<=0) {
    strcpy(temp[entnum].last,book.entries[entnum].last);
    strcpy(temp[entnum].first,book.entries[entnum].first);
    strcpy(temp[entnum].area,book.entries[entnum].area);
    strcpy(temp[entnum].number,book.entries[entnum].number);
    strcpy(temp[entnum].des,book.entries[entnum].des);
    entnum++;
  }
  strcpy(temp[entnum].last,entry->last);
  strcpy(temp[entnum].first,entry->first);
  strcpy(temp[entnum].area,entry->area);
  strcpy(temp[entnum].number,entry->number);
  strcpy(temp[entnum].des,entry->des);
  entnum++;
  book.size++;
  for (;entnum<book.size;entnum++) {
    strcpy(temp[entnum].last,book.entries[entnum-1].last);
    strcpy(temp[entnum].first,book.entries[entnum-1].first);
    strcpy(temp[entnum].area,book.entries[entnum-1].area);
    strcpy(temp[entnum].number,book.entries[entnum-1].number);
    strcpy(temp[entnum].des,book.entries[entnum-1].des);
  }
  delete (book.entries);
  book.entries=temp;
}

void delentry(booktype &book,int &line) {
  int i;
  for (i=line;i<book.size-1;i++) {
    strcpy(book.entries[i].last,book.entries[i+1].last);
    strcpy(book.entries[i].first,book.entries[i+1].first);
    strcpy(book.entries[i].area,book.entries[i+1].area);
    strcpy(book.entries[i].number,book.entries[i+1].number);
    strcpy(book.entries[i].des,book.entries[i+1].des);
  }
  book.size--;
  if (line>=book.size) line=book.size-1;
}
#include "phone.h"
#include "scrio.h"
#include "fileio.h"


void main() {
  booktype book;
  int c;
  int line=0;
  scrinit();
  open(book,defaultbook);
  pbrefresh(book,line);
  while ((c=getkey())!='q') {
    switch (c) {
     case 'a': { add(book); pbrefresh(book,line); break; }
     case 'd': { del(book,line); pbrefresh(book,line); break; }
     case scrupkey: { scrup(book,line); break; }
     case scrdnkey: { scrdn(book,line); break; }
     case 'r': { pbrefresh(book,line); break; }
    }
  }
  save(book,defaultbook);
  scrshut();
}
#include <curses.h>
#include <stdio.h>
#include <stdarg.h>

#include "phone.h"
#include "scrio.h"
#include "list.h"
/* #include "list2.h" */

void error(char *s,...) {
  WINDOW *errwin;
  char errstr[81];
  errwin=newwin(1,80,12,0);
  va_list arg;
  va_start(arg,s);
  vsprintf(errstr,s,arg);
  wprintw(errwin,errstr);
  va_end(arg);
  wrefresh(errwin);
  delwin(errwin);
}

void scrinit() {
  initscr();
  start_color();
  init_pair(1,7,4);
  init_pair(2,6,4);
  init_pair(3,7,0);
  raw();
  noecho();
  keypad(stdscr,TRUE);
}

void scrshut() {
  clear();
  refresh();
  noraw();
  endwin();
}

void wclrscr(WINDOW *win) {
  int r,c;
  wmove(win,0,0);
  for (r=0;r<=win->_maxy;r++) {
    for (c=0;c<=win->_maxx;c++) {
      wprintw(win," ");
    }
  }
  wmove(win,0,0);
}

void wclreol(WINDOW *win) {
  int oldx,oldy,c;
  oldx=win->_curx;
  oldy=win->_cury;
  for (c=oldx;c<=win->_maxx;c++) {
    wprintw(win," ");
  }
  wmove(win,oldy,oldx);
}

#define clreol() wclreol(stdscr)

void add(booktype &book) {
  WINDOW *addwin;
  entrytype *newent;
  addwin=newwin(6,30,10,10);
  newent=new entrytype;
  echo();
  wattrset(addwin,COLOR_PAIR(1));
  wclrscr(addwin);
  wprintw(addwin,"First name: ");
  wrefresh(addwin);
  wgetnstr(addwin,newent->first,firstmax);
  wprintw(addwin,"Last name: ");
  wrefresh(addwin);
  wgetnstr(addwin,newent->last,lastmax);
  wprintw(addwin,"Area code: ");
  wrefresh(addwin);
  wgetnstr(addwin,newent->area,areamax);
  wprintw(addwin,"Phone number: ");
  wrefresh(addwin);
  wgetnstr(addwin,newent->number,numbermax);
  wprintw(addwin,"Description: ");
  wrefresh(addwin);
  wgetnstr(addwin,newent->des,desmax);
  delwin(addwin);
  noecho();
  addentry(book,newent);
  delete newent;
}

void del(booktype &book,int &line) {
  WINDOW *win=newwin(10,30,10,10);
  char in[256];
  wattrset(win,COLOR_PAIR(1));
  echo();
  wclrscr(win);
  wprintw(win,"Delete %s %s\n",book.entries[line].first,book.entries[line].last);
  wprintw(win,"Are you sure (y,n): ");
  wrefresh(win);
  wgetstr(win,in);
  if (in[0]=='y') {
    delentry(book,line);
  }
  noecho();
  delwin(win);
}

void scrup(booktype &book,int &line) {
  if (line>0) line--;
  pbrefresh(book,line);
}

void scrdn(booktype &book,int &line) {
  if (line<book.size-1) line++;
  pbrefresh(book,line);
}

void pbrefresh(booktype &book,int line) {
  int i;
  clear();
  move(0,0);
  attrset(COLOR_PAIR(3));
  for (i=line;i<book.size && i-line<24;i++) {
    wprintw(stdscr,"%-*s %-*s (%*s)%*s %-*s\n",lastmax,book.entries[i].last,
	    firstmax,book.entries[i].first,areamax,book.entries[i].area,
	    numbermax,book.entries[i].number,desmax,book.entries[i].des);
  }
  move(24,0);
  attrset(COLOR_PAIR(2));
  wprintw(stdscr,"(a)dd (d)elete (q)uit ([) page up (]) page down");
  clreol();
  wrefresh(stdscr);
}

int getkey() {
  return(getch());
}
#ifndef FILEIO_H
#define FILEIO_H

void save(booktype &,char *);
void open(booktype &,char *);

#endif //FILEIO_H
#ifndef LIST_H
#define LIST_H

#include "phone.h"

 #include "constants.h" /* my constants */
#include <set.h> // set prototypes
/* socket stuff */   #include <sockets.h>
/* some mumbo jumbo */   #include "mj.h"  // did I need this?
/* helper functions */   #include "helper.h"  /* these come in handy */

void addentry(booktype &book,entrytype *entry);
void delentry(booktype &book,int &line);

#endif //LIST_H
#ifndef PHONE_H
#define PHONE_H

#define firstmax  15
#define lastmax   15
#define areamax    3
#define numbermax  7
#define desmax    33
#define defaultbook "phbk.dat"

struct entrytype {// +1's are for the NULL character
  char first[firstmax+1];
  char last[lastmax+1];
  char area[areamax+1];
  char number[numbermax+1];
  char des[desmax+1];
};

struct booktype {
  entrytype *entries;
  int size;
  char *fname;
};

#endif //PHONE_H
#ifndef SCRIO_H
#define SCRIO_H

// #include <ncurses.h>
#include <curses.h>
// #include <cursesn.h>

#define scrupkey KEY_UP
#define scrdnkey KEY_DOWN

void error(char *s,...);
void scrinit();
void scrshut();
void add(booktype &book);
void del(booktype &book,int &line);
void scrup(booktype &book,int &line);
void scrdn(booktype &book,int &line);
void pbrefresh(booktype &book,int line);
int getkey();

#endif //SCRIO_H
  /* #include<stdio.h> */
#include<string.h>  /* #include<stdio.h> */
 /* #include<stdio.h> */ #include<math.h>

// #include "list.h" #include<stdio.h>

#include "queue.h" // this is a comment
