#ifndef LIST_H
#define LIST_H

#include "phone.h"

 #include "constants.h" /* my constants */
#include <set.h> // set prototypes
/* socket stuff */   #include <sockets.h>
/* some mumbo jumbo */   #include "mj.h"  // did I need this?
/* helper functions */   #include "helper.h"  /* these come in handy */

void addentry(booktype &book,entrytype *entry);
void delentry(booktype &book,int &line);

#endif //LIST_H
