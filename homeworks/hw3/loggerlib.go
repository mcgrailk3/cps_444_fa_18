/**********************************************************
/
/      filename:   loggerlib.go
/
/   description:   Program is an implementation of a
/                  logging utility that allows the caller
/                  to save a message at the end of a list
/                  along with the time.
/
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #3
/
/      assigned:   September 6, 2018
/           due:   September 13, 2018
/
/ **********************************************************/

package loggerlib

import (
	"errors"
	"os"
	"strings"
	"time"
)

type Data_t struct {
	Logged_time time.Time
	Str         string
}

type Log_t struct {
	item Data_t
	next *Log_t
}

// global, private variables
var headptr *Log_t
var tailptr *Log_t

func gettime(runtime time.Time) string {
	var timestr string

	var months map[string]string

	months = make(map[string]string)
	months["Jan"] = "01"
	months["Feb"] = "02"
	months["Mar"] = "03"
	months["Apr"] = "04"
	months["May"] = "05"
	months["Jun"] = "06"
	months["Jul"] = "07"
	months["Aug"] = "08"
	months["Sep"] = "09"
	months["Oct"] = "10"
	months["Nov"] = "11"
	months["Dec"] = "12"
	const layout = "Jan 2 2006 15:04:05"
	timeslice := strings.Split(runtime.Format(layout), " ")
	timestr = months[timeslice[0]] + "/"
	if len(timeslice[1]) == 1 {
		timestr += "0"
	}
	timestr += timeslice[1] + "/" + timeslice[2] + " " + timeslice[3]
	return timestr
}

func Addmsg(data Data_t) (int, error) {
	newnode := Log_t{data, nil}

	if headptr == nil {
		headptr = &newnode
	} else {
		tailptr.next = &newnode
	}
	tailptr = &newnode
	if &newnode == nil {
		return -1, errors.New("Unable to add message")
	} else {
		return 0, nil
	}
}

func Clearlog() {
	headptr = nil
	tailptr = nil
}

func Getlog() (string, error) {
	var log string
	var temphead *Log_t
	temphead = headptr
	if temphead == nil && tailptr == nil {
		temphead = nil
		return "", errors.New("No log available!")
	} else {
		for log = ""; temphead != nil; temphead = temphead.next {
			log = log + "Time: " + gettime(temphead.item.Logged_time) + "\n"
			log = log + "Message: " + temphead.item.Str + "\n\n"
		}
		return log, nil
	}
}

func Savelog(filename string) error {
	var err error
	if filename == "" {
		return nil
	}
	if headptr == nil {
		return errors.New("No history available")
	} else {
		f, err := os.Create(filename)
		if err != nil {
			return err
		} else {
			defer f.Close()
			log1, err2 := Getlog()
			if err2 != nil {
				return err2
			} else {
				f.WriteString(log1)
			}
		}
	}
	if err != nil {
		return nil
	} else {
		return err
	}
}
