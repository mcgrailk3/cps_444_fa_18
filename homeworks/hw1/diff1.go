/**********************************************************
/
/      filename:   diff1 .go
/
/   description:   Program acts like the linux command diff 
/                  by using the golang. It can take input as
/                  files or command line args.
/            
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #1
/
/      assigned:   August 23, 2018
/           due:   August 30, 2018
/
/ **********************************************************/

package main

import (
   "flag"
   "fmt"
   "bufio"
   "os"
//   "io"
   "strings"
)

func main() {

   //Establish flags
   lPtr := flag.Bool("l", false, "the l option does ... um ... something")
   tPtr := flag.Bool("t", false, "the t option does ... um ... something")
   mPtr := flag.Bool("m", false, "the m option does ... um ... something")
   aPtr := flag.Bool("a", false, "the a option does ... um ... something")
   flag.Parse()
   file1,err1 := os.Open(flag.Args()[1])
   file2,err2 := os.Open(flag.Args()[2])

   if err1 != nil {
      fmt.Fprintf(os.Stderr, "invalid file name %s\n", os.Args[1])
      os.Exit(2)
   }
   if err2 != nil {
      fmt.Fprintf(os.Stderr, "invalid file name %s\n", os.Args[2])
      os.Exit(2)
   }
   if len(flag.Args())>2 {
      fmt.Fprintf(os.Stderr, "extra operand ")

      os.Exit(2)
   }
   //(str1, " \t") trimleftright
   scanner1 := bufio.NewScanner(strings.NewReader(file1))
   scanner2 := bufio.NewScanner(strings.NewReader(file2))
   input1 := scanner1
   input2 := scanner2
   err3 := scanner1.Err()
   err4 := scanner2.Err()
   //Gives everything but the flags

   lineNum := 1
   //Strings.trimleft, right, middle, strings.newreplacer, trimspace
   //If true... or on... or used
   for linenum := 1; err3 != nil && err4 != nil; linenum++ {
      
      //ignore leading whitespace
      if *lPtr && !*aPtr {
        input1 = strings.TrimLeft(input1, ' \t')
        input2 = strings.TrimLeft(input2, ' \t')
        if input1 != input2 {
           fmt.Fprintf(os.Stderr, "%d", lineNum)
        }
      }
      //ignore trailing whitespace new line
      if *tPtr && !*aPtr {
         input1 = strings.TrimRight(input1, ' \n')
         input2 = strings.TrimRight(input2, ' \n')
         if input1 != input2 {
            fmt.Fprintf(os.Stderr, "%d", lineNum)
         }

      }
      //ignore intermediary whitespace
      if *mPtr && !*aPtr {
         input1 = strings.Join(input1, ' \t')
         input2 = strings.Join(input2, ' \t')
         if input1 != input2 {
            fmt.Fprintf(os.Stderr, "%d", lineNum)
         }

      }
      //ignore all whitespace
      if *aPtr && !*mPtr && !*lPtr && !*mPtr {
         input1 = Strings.TrimLeft(input1, ' \t')
         input2 = Strings.TrimLeft(input2, ' \t')
         if input1 != input2 {
            fmt.Fprintf(os.Stderr, "%d", lineNum)
         }

      }
      if *aPtr && (*mPtr ||*lPtr || *tPtr) {
         fmt.Fprintf(os.Stderr, "Option -a cannot be combined with any other options")
         os.Exit(9)
      } else {
         if input1 != input2 {
            fmt.Fprintf(os.Stderr, "%d", lineNum)
         }
      }

   }

}
