package logapp_helperfuns

// THIS FILE MUST NOT BE MODIFIED

import (
   "fmt"
   "time"
   "os/exec"
   "loggerlib/loggerlib")

/* execute cmd store time and cmd in history list */
func Execmd (cmd string) (int,error) {
   execute := new(loggerlib.Data_t)
   execute.Logged_time = time.Now().Local()
   execute.Str = cmd

   cmdp := exec.Command("ksh", "-c", cmd)
   cmdpOut, err := cmdp.Output()

   /* command could not be executed */
   if err != nil {
      panic(err)
   }
   fmt.Print(string(cmdpOut))

   return loggerlib.Addmsg (*execute)
}

/* write the history list to stdout */
func Displayhist() {
   log,err := loggerlib.Getlog()
   if (err == nil) {
      fmt.Printf ("%s", log)
   }
}

/* write the history list to file with name file */
func Savehistory (filename string) {
   loggerlib.Savelog (filename)
}

/*  clears the history list */
func Clearhistory() {
   loggerlib.Clearlog()
}