/**********************************************************
/
/      filename:   barrierserver.go
/
/   description:   Operates the server side of pipe
/                  communication for homework 6.
/
/        author:   McGrail, Kevin
/      login id:   FA_18_CPS444_14
/
/         class:   CPS 444
/    instructor:   Perugini
/    assignment:   Homework #6
/
/      assigned:   October 11, 2018
/           due:   October 18, 2018
/
/ **********************************************************/

package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"syscall"
)

func handle(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Server handle\n")
		log.Fatal(err)
	}
}

func main() {

	const request = uint32(syscall.S_IRUSR | syscall.S_IWUSR | syscall.S_IWGRP | syscall.S_IWOTH)
	const release = uint32(syscall.S_IWUSR | syscall.S_IRUSR | syscall.S_IRGRP | syscall.S_IROTH)
	src1 := "/home/FA_18_CPS444_14/go/src/hw6/"
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "No enough args...exiting\n")
		os.Exit(1)
	}
	name := os.Args[1]
	size, err := strconv.Atoi(os.Args[2])
	handle(err)

	if name == "" {
		fmt.Fprintf(os.Stderr, "Invalid name...exiting\n")
		os.Exit(1)
	}

	if size < 1 {
		fmt.Fprintf(os.Stderr, "Invalid size...exiting\n")
		os.Exit(1)
	}

	handle(syscall.Mkfifo(src1+name+".request", 0777))
	handle(syscall.Mkfifo(src1+name+".release", 0777))

	data := []byte{1}
	written := make([]byte, size)
	for true {

		//request, err := syscall.Open(src1+name+".request", syscall.O_RDONLY, 0)
		//fmt.Fprintf(os.Stderr, "69\n")
		//handle(err)

		//count, err := syscall.Read(request, data)
		//handle(err)
		for i := 0; i < size; i++ {
			//fmt.Fprintf(os.Stderr, "%d \n", i)
			request, err := syscall.Open(src1+name+".request", syscall.O_RDONLY, 0)
			//fmt.Fprintf(os.Stderr, "69\n")
			handle(err)
			count, err := syscall.Read(request, data)
			handle(err)
			//fmt.Fprintf(os.Stderr, "%d \n", count)

			if count != 0 {
				//fmt.Fprintf(os.Stderr, "Written: %d \n", i)
				written[i] = byte('g')
				//fmt.Fprintf(os.Stderr, "74\n")
				handle(err)
				//fmt.Fprintf(os.Stderr, "77\n")
				fmt.Fprintf(os.Stderr, "server read %s in %d byte.\n", string(written[i]), len(data))

			}
			err = syscall.Close(request)
			//fmt.Fprintf(os.Stderr, "83\n")

			handle(err)
			//fmt.Fprintf(os.Stderr, "here55555 \n")
		}

		//fmt.Fprintf(os.Stderr, "TEST server read %s .\n", string(written[:]))
		//err = syscall.Close(request)
		//fmt.Fprintf(os.Stderr, "83\n")
		//	handle(err)
		fmt.Fprintf(os.Stderr, "here0 \n")

		release, err := syscall.Open(src1+name+".release", syscall.O_WRONLY, 0)
		//fmt.Fprintf(os.Stderr, "87\n")
		fmt.Fprintf(os.Stderr, "here00 \n")

		handle(err)
		for i := 0; i < size; i++ {
			//release, err := syscall.Open(src1+name+".release", syscall.O_WRONLY, 0)
			fmt.Fprintf(os.Stderr, "87\n")
			//handle(err)
			fmt.Fprintf(os.Stderr, "here \n")
			n, err := syscall.Write(release, data)
			fmt.Fprintf(os.Stderr, "91\n")
			handle(err)
			if n < 1 {
				//fmt.Fprintf(os.Stderr, "95\n")
				handle(err)
			}

			//err = syscall.Close(release)
		}
		//fmt.Fprintf(os.Stderr, "TEST server wrote %s .\n", string(written[:]))
		//fmt.Fprintf(os.Stderr, "here1 \n")

		fmt.Fprintf(os.Stderr, "server just wrote %s in %d bytes to release pipe\n", string(written[:]), len(written))
		written = make([]byte, size)
		err = syscall.Close(release)
		//fmt.Fprintf(os.Stderr, "here2 \n")

		//fmt.Fprintf(os.Stderr, "100\n")
		handle(err)
		fmt.Fprintf(os.Stderr, "server just closed the release pipe.\n")
	}
	return
}
