set ruler showmode showmatch ts=3 noexpandtab
syntax on
map  <CR>:!aspell --dont-backup check %<CR>:e! %<CR>
nmap <C-J> vipgq
